
========================================
|    Version 0.6 - "Sprite Monster"    |
|    UNRELEASED                        |
========================================

= Features =

- DMA queue for transferring animation updates over several frames.
- VRAM slot allocation system
- Integration of the DMA queue and VRAM allocation for easy use by objects.
- Undo/redo in the editor!
- Editor now warns if you try to quit without saving your changes.
- New AddMetaspriteBoundingBox routine which does an overall bounding box check before drawing.
- New DespawnObjectIfOutside routine which removes objects outside the screen.
- New cleanup callback feature to allow objects to run a function when deleted.
- New region in HiRAM that is erased between scenes, just like LoRAM. Use thoughtfully.
- DeleteObject no longer needs to know the pool the object came from.
- Input handling for controller 2
- Button release input
- Added 32-bit scene tic counter
- RAM mirrors of BG 2/3 scroll
- New example: "metaspritebounds"
- New example: "objectpools"

= Bugfixes =

- Sprites correctly moved off-screen at start, even when the scene doesn't call FinalizeSprites
- Use 8-bit write for incrementing/decrementing high byte in MoveObject to avoid overflows

= API Changes =

- DMAMakeStrip4 was removed, as it is no longer necessary.
- DMA strip table adds must use the dmaTableStrip variables instead of dmaTable.
- AddSprite and AddSpriteFast have been renamed to AddMetasprite and AddSimpleSprite, respectively.
- CODExLO and CODExHI renamed to DATAx and CODEx.
- Jmp_AnimateDrawSimpleThenNext has been changed to AnimateDrawSimpleSprite.
- Sprite scrolling decoupled from BG1 scrolling.
- Scene init and think functions are now called with registers initially set to 16-bit

= Other =

- Added table in manual illustrating supported banks for different kinds of code/data.
- Buildsystem saves the .sym and .memmap files to the same filename as the output .sfc for quick debugging.
- Some small adjustments to the MoveObject routine
- Optimizations to stripped DMA transfers
- Removed RandomSigned, since it's not really needed.


========================
|    Version 0.5.2b    |
========================

= Features =

- Added a "create blank raw file" feature to the editor. Press N on the initial screen to use it.
- Added the ability to paint tilemaps with the mouse, plus a visual tile picker screen, to the tilemap editor.

= Bugfixes =

- Decorated the top of each function in the engine and example source to improve readability.


=======================
|    Version 0.5.1    |
=======================

= Bugfixes =

- Fixed directory separators for the buildsystem on native Windows CMD
- Minor documentation fixes


=======================================
|    Version 0.5 - "Flying Kobold"    |
=======================================

- Initial release
