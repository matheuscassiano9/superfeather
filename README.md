Superfeather SNES Game Engine
====

Make fast SNES games in assembly. Performant. Flexible. Convenient.

Note that this git repo does not contain any tool binaries. Those are shipped with releases.

## License ##

Superfeather uses the zlib License. See LICENSE for more details.
