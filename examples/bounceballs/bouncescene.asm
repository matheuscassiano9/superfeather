
.p816   ; 65816 processor
.a16
.i16
.smart

; Most source files should start with the above series of directives. This tells ca65 that we are
;  programming for the 65816 and that it should track use of rep/sep to determine the register sizes,
;  but that they should default to 16-bit.

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/dma.inc"
.include "../../inc/engine/graphics.inc"

.import BallChar, BallChar_Len, SpawnBall, Ball_Small, Ball_Large
.import SetupPpuRegisters:far, BeginSprites, UpdateObjects, FinalizeSprites, screenBrightness, inputRaw, slowdown
.import PrintHexByteToTilemap, DMATryAdd, dmaTableSrc, dmaTableSrcBank, dmaTableDest

.export BounceSceneInit, BounceSceneThink

.segment "DATA2"

; Various data files, mostly uploading during scene initialization.

BouncePal:
    .INCBIN "examples/bounceballs/data/bounceballs.pal"
BouncePal_Len = * - BouncePal

HudChar:
    .INCBIN "examples/bounceballs/data/hud.pic2"
HudChar_Len = * - HudChar

HudTilemap:
    .INCBIN "examples/bounceballs/data/hud.map"
HudTilemap_Len = * - HudTilemap

BackgroundChar:
    .INCBIN "examples/bounceballs/data/superfeatherbg.pic"
BackgroundChar_Len = * - BackgroundChar

BackgroundTilemap:
    .INCBIN "examples/bounceballs/data/superfeatherbg.map"
BackgroundTilemap_Len = * - BackgroundTilemap

.segment "LORAM"
; For the CPU usage meter
lastScanline: .res 2
lastSlowdown: .res 2
; RAM buffer of the HUD contents, which we transfer to VRAM later.
hudTilemapBuffer: .res 128

.segment "CODE0"

; These values are used to initialize the PPU registers, so we can set up the background mode and
;  address offsets we need, among other things. See documentation on the PPU registers for more
;  information.

;                            OBJSEL,                                BGMODE,    BG1TILE,                                      BG2TILE,                                      BG3TILE,                                      BG4TILE,   BG12CHAR,                                      BG34CHAR,                                      MODE7SEL,  WIN12SEL,  WIN34SEL,  WINOBJSEL, WINBGLOG,  WINOBJLOG, DESTMAIN,  DESTSUB,   WINMAIN,   WINSUB,    CMATHSEL,  CMATHDEST, FIXEDCOLOR,        SETINI
Define_PpuDef SceneInitRegs, PPU_OBJSIZE_8_16 | PPU_OBJCHARADDR_0,  %00001001, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6000, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_6800, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_7800, %00000000, PPU_BG13CHARADDR_3000 | PPU_BG24CHARADDR_3000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00011111, %00000000, %00000000, %00000000, %00000000, %00000000, %0000000000000000, %00000000


; ============================================================================
; The initialization function, called once during startup of this scene
; ============================================================================
BounceSceneInit:
    php
    
    rep #$10
    sep #$20
    
    ; Initialize the registers using the data structure above.
    ldx #.loword(SceneInitRegs)
    jsl SetupPpuRegisters

    ; Load palettes, VRAM data, etc. here.
    Macro_LoadPalette BouncePal, #0, #BouncePal_Len
    Macro_LoadVRAM BallChar, #$0000, #BallChar_Len
    Macro_LoadVRAM HudChar, #$2000, #HudChar_Len
    Macro_LoadVRAM HudTilemap, #$7800, #HudTilemap_Len
    Macro_LoadVRAM BackgroundChar, #$3000, #BackgroundChar_Len
    Macro_LoadVRAM BackgroundTilemap, #$6000, #BackgroundTilemap_Len
    
    Macro_LoadWRAM HudTilemap, hudTilemapBuffer, #HudTilemap_Len
    
    plp
    rts


; ============================================================================
; The thinker function for this scene, called every frame.
; ============================================================================
BounceSceneThink:
    php
    
    ; Turn on the screen
    sep #$20
    lda #%00001111
    sta screenBrightness
    rep #$30
    
    ; Try to schedule the transfer of the HUD tilemap buffer to VRAM
    lda #$80
    jsr DMATryAdd
    ; Was it added to the queue?
    bvs _NoDMA
    ; It was, set the source/dest of the transfer.
    lda #hudTilemapBuffer
    sta dmaTableSrc, x
    lda #$80
    sta dmaTableSrcBank, x
    lda #$7800
    sta dmaTableDest, x
    
    ; If we have slowdown, set the CPU usage to full
    lda lastSlowdown
    bne _FullCpu
    
    ; Load the vertical scanline position from the end of last frame
    ldx lastScanline
    ; Translate from scanline position to CPU usage number
    lda _ScanlineToCpuUsage, x
    and #$ff
    tax
    bra _PrintCpu
    
_FullCpu:
    ldx #$99

_PrintCpu:
    ; Y is the address of our tile offset that we want to write the number to
    ldy #hudTilemapBuffer + $4c
    jsr PrintHexByteToTilemap

_NoDMA:
    jsr BeginSprites
    
    ; Check if we're pressing any buttons on the controller
    lda inputRaw
    beq _NoInput
    ; If we are, spawn two objects, one of each kind.
    ldy #.loword(Ball_Large)
    jsr SpawnBall
    ldy #.loword(Ball_Small)
    jsr SpawnBall
    
_NoInput:
    ; Allow the objects to run their thinkers
    jsr UpdateObjects
    jsr FinalizeSprites
    
    ; Read the current vertical scanline position
    sep #$20
    lda $2137
    lda $213d
    xba
    lda $213d
    xba
    
    ; Only the first 9 bits are valid. Trim the rest out
    rep #$30
    and #$1ff
    ; Store it for the next frame
    sta lastScanline
    lda slowdown
    sta lastSlowdown
    
    plp
    rts


; ============================================================================
; This lookup table translates a vertical scanline position to a decimal-encoded
;  CPU percentage. VBlank begins (and therefore the frame ends) at scanline 225.
;  The scanline position wraps around at 261, but if interlace mode is enabled, this
;  value may be 262 instead, so we cover that case as well.
; ============================================================================
_ScanlineToCpuUsage:
    .byte $14, $14, $14     ; 0-2
    .byte $15, $15          ; 3-4
    .byte $16, $16, $16     ; 5-7
    .byte $17, $17, $17     ; 8-10
    .byte $18, $18          ; 11-12
    .byte $19, $19, $19     ; 13-15
    .byte $20, $20, $20     ; 16-18
    .byte $21, $21          ; 19-20
    .byte $22, $22, $22     ; 21-23
    .byte $23, $23          ; 24-25
    .byte $24, $24, $24     ; 26-28
    .byte $25, $25, $25     ; 29-31
    .byte $26, $26          ; 32-33
    .byte $27, $27, $27     ; 34-36
    .byte $28, $28          ; 37-38
    .byte $29, $29, $29     ; 39-41
    .byte $30, $30, $30     ; 42-44
    .byte $31, $31          ; 45-46
    .byte $32, $32, $32     ; 47-49
    .byte $33, $33, $33     ; 50-52
    .byte $34, $34          ; 53-54
    .byte $35, $35, $35     ; 55-57
    .byte $36, $36          ; 58-59
    .byte $37, $37, $37     ; 60-62
    .byte $38, $38, $38     ; 63-65
    .byte $39, $39          ; 66-67
    .byte $40, $40, $40     ; 68-70
    .byte $41, $41, $41     ; 71-73
    .byte $42, $42          ; 74-75
    .byte $43, $43, $43     ; 76-78
    .byte $44, $44          ; 79-80
    .byte $45, $45, $45     ; 81-83
    .byte $46, $46, $46     ; 84-86
    .byte $47, $47          ; 87-88
    .byte $48, $48, $48     ; 89-91
    .byte $49, $49          ; 92-93
    .byte $50, $50, $50     ; 94-96
    .byte $51, $51, $51     ; 97-99
    .byte $52, $52          ; 100-101
    .byte $53, $53, $53     ; 102-104
    .byte $54, $54, $54     ; 105-107
    .byte $55, $55          ; 108-109
    .byte $56, $56, $56     ; 110-112
    .byte $57, $57          ; 113-114
    .byte $58, $58, $58     ; 115-117
    .byte $59, $59, $59     ; 118-120
    .byte $60, $60          ; 121-122
    .byte $61, $61, $61     ; 123-125
    .byte $62, $62, $62     ; 126-128
    .byte $63, $63          ; 129-130
    .byte $64, $64, $64     ; 131-133
    .byte $65, $65          ; 134-135
    .byte $66, $66, $66     ; 136-138
    .byte $67, $67, $67     ; 139-141
    .byte $68, $68          ; 142-143
    .byte $69, $69, $69     ; 144-146
    .byte $70, $70, $70     ; 147-149
    .byte $71, $71          ; 150-151
    .byte $72, $72, $72     ; 152-154
    .byte $73, $73          ; 155-156
    .byte $74, $74, $74     ; 157-159
    .byte $75, $75, $75     ; 160-162
    .byte $76, $76          ; 163-164
    .byte $77, $77, $77     ; 165-167
    .byte $78, $78          ; 168-169
    .byte $79, $79, $79     ; 170-172
    .byte $80, $80, $80     ; 173-175
    .byte $81, $81          ; 176-177
    .byte $82, $82, $82     ; 178-180
    .byte $83, $83, $83     ; 181-183
    .byte $84, $84          ; 184-185
    .byte $85, $85, $85     ; 186-188
    .byte $86, $86          ; 189-190
    .byte $87, $87, $87     ; 191-193
    .byte $88, $88, $88     ; 194-196
    .byte $89, $89          ; 197-198
    .byte $90, $90, $90     ; 199-201
    .byte $91, $91, $91     ; 202-204
    .byte $92, $92          ; 205-206
    .byte $93, $93, $93     ; 207-209
    .byte $94, $94          ; 210-211
    .byte $95, $95, $95     ; 212-214
    .byte $96, $96, $96     ; 215-217
    .byte $97, $97          ; 218-219
    .byte $98, $98, $98     ; 220-222
    .byte $99, $99, $99     ; 223-225
    .byte $00, $00          ; 226-227
    .byte $01, $01, $01     ; 228-230
    .byte $02, $02          ; 231-232
    .byte $03, $03, $03     ; 233-235
    .byte $04, $04, $04     ; 236-238
    .byte $05, $05          ; 239-240
    .byte $06, $06, $06     ; 241-243
    .byte $07, $07          ; 244-245
    .byte $08, $08, $08     ; 246-248
    .byte $09, $09, $09     ; 249-251
    .byte $10, $10          ; 252-253
    .byte $11, $11, $11     ; 254-256
    .byte $12, $12, $12     ; 257-259
    .byte $13, $13, $13     ; 260-262
    
