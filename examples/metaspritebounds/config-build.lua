
return
{
    fileList =
    {
        -- Engine files
        "src/engine/init.asm",
        "src/engine/utils.asm",
        "src/engine/initsnes.asm",
        "src/engine/crashhandler.asm",
        "src/engine/gameloop.asm",
        "src/engine/audio.asm",
        "src/engine/dma.asm",
        "src/engine/graphics.asm",
        "src/engine/dmamanager.asm",
        "src/engine/gameobjects.asm",
        
        -- Game files
        "examples/metaspritebounds/header.asm",
        "examples/metaspritebounds/main.asm",
        "examples/metaspritebounds/spriteboundscene.asm",
        "examples/metaspritebounds/burst.asm",
    },
    target = "superfeather-ex-metaspritebounds",
    linkerConfig = "examples/config-ca65.cfg"
}
