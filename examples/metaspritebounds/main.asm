
.p816   ; 65816 processor
.a16
.i16
.smart

; Most source files should start with the above series of directives. This tells ca65 that we are
;  programming for the 65816 and that it should track use of rep/sep to determine the register sizes,
;  but that they should default to 16-bit.

; Import/export statements are how you access symbols defined in other modules.

.import Gameloop, sceneInitFunc, sceneThinkFunc, SpriteBoundSceneInit, SpriteBoundSceneThink

.export GameMain, SpcDriver

.segment "DATA3"

; The symbol SpcDriver must be defined somewhere, or the linker will complain. It is used internally
;  by the engine, but since this example has no sound, we just have a driver with blank data.

SpcDriver:
    .incbin "examples/metaspritebounds/data/spc700.bin"

.segment "CODE0"


; ============================================================================
; GameMain is called once the engine has finished its own initialization. This space is for your game
;  to perform its own setup.
; ============================================================================
GameMain:
    
    ; Set your initial scenes here by writing to sceneInitFunc and sceneThinkFunc
    lda #.loword(SpriteBoundSceneInit)
    sta sceneInitFunc
    lda #.loword(SpriteBoundSceneThink)
    sta sceneThinkFunc

_Forever:
    ; Gameloop is an engine internal function. We jump there once our game initialization is done.
    jsr Gameloop
    bra _Forever
