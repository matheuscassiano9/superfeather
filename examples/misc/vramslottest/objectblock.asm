
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../../inc/engine/constants.inc"
.include "../../../inc/engine/graphics.inc"
.include "../../../inc/engine/gameobjects.inc"

.import objThinker, objAnimDef, objPosX, objPosY, objCustomA, AddMetasprite, EmptyThinker
.export SpawnBlock, DespawnBlock, BlockThinker

objBlockSize = objCustomA

.segment "LORAM"

_scratch: .res 2

.segment "CODE0"

;                     Name,                X pos, Y pos, sprite size,  tile, vhoopppN, next sprite
Define_SpriteDef      Metasprite_Block1,   $0000, $0000, SPRITE_LARGE, $00, %00100000, 0

Define_SpriteDef      Metasprite_Block2a,  $0000, $0000, SPRITE_LARGE, $04, %00100000, Metasprite_Block2b
Define_SpriteDef      Metasprite_Block2b,  $0020, $0000, SPRITE_LARGE, $04, %01100000, 0

Define_SpriteDef      Metasprite_Block3a,  $0000, $0000, SPRITE_LARGE, $04, %00100000, Metasprite_Block3b
Define_SpriteDef      Metasprite_Block3b,  $0020, $0000, SPRITE_LARGE, $05, %00100000, Metasprite_Block3c
Define_SpriteDef      Metasprite_Block3c,  $0040, $0000, SPRITE_LARGE, $04, %01100000, 0

Define_SpriteDef      Metasprite_Block4a,  $0000, $0000, SPRITE_LARGE, $04, %00100000, Metasprite_Block4b
Define_SpriteDef      Metasprite_Block4b,  $0020, $0000, SPRITE_LARGE, $05, %00100000, Metasprite_Block4c
Define_SpriteDef      Metasprite_Block4c,  $0040, $0000, SPRITE_LARGE, $05, %00100000, Metasprite_Block4d
Define_SpriteDef      Metasprite_Block4d,  $0060, $0000, SPRITE_LARGE, $04, %01100000, 0


_BlockSizeToSpritedef:
    .word .loword(Metasprite_Block1)
    .word .loword(Metasprite_Block2a)
    .word .loword(Metasprite_Block3a)
    .word .loword(Metasprite_Block4a)

_BlockIdToX:
    .word $0000
    .word $0020
    .word $0040
    .word $0060
    .word $0080
    .word $00a0
    .word $00c0
    .word $00e0
    .word $0000
    .word $0020
    .word $0040
    .word $0060
    .word $0080
    .word $00a0
    .word $00c0
    .word $00e0
    .word $0000
    .word $0020
    .word $0040
    .word $0060
    .word $0080
    .word $00a0
    .word $00c0
    .word $00e0
    .word $0000
    .word $0020
    .word $0040
    .word $0060
    .word $0080
    .word $00a0
    .word $00c0
    .word $00e0

_BlockIdToY:
    .word $0040
    .word $0040
    .word $0040
    .word $0040
    .word $0040
    .word $0040
    .word $0040
    .word $0040
    .word $0068
    .word $0068
    .word $0068
    .word $0068
    .word $0068
    .word $0068
    .word $0068
    .word $0068
    .word $0090
    .word $0090
    .word $0090
    .word $0090
    .word $0090
    .word $0090
    .word $0090
    .word $0090
    .word $00b8
    .word $00b8
    .word $00b8
    .word $00b8
    .word $00b8
    .word $00b8
    .word $00b8
    .word $00b8


SpawnBlock:
    ; Convert slot ID to block ID (we aren't using CreateObject in this example)
    sta _scratch
    clc
    asl
    adc _scratch
    tax
    
    lda #.loword(BlockThinker)
    sta objThinker, x
    tya
    sta objBlockSize, x
    lda _BlockSizeToSpritedef, y
    sta objAnimDef, x
    lda _scratch
    asl
    tay
    lda _BlockIdToX, y
    sta objPosX, x
    lda _BlockIdToY, y
    sta objPosY, x
    
    rts


DespawnBlock:
    ; Convert slot ID to block ID (we aren't using CreateObject in this example)
    tya
    sta _scratch
    clc
    asl
    adc _scratch
    tax
    
    lda #.loword(EmptyThinker)
    sta objThinker, x
    ldy objBlockSize, x
    
    rts
    

; ============================================================================
; The thinker function associated with this object. Called once a frame for
;  all objects of this type.
; ============================================================================
BlockThinker:
    ldy objAnimDef, x
    jsr AddMetasprite
    
    Macro_NextThinker
