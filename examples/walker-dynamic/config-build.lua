
return
{
    fileList =
    {
        -- Engine files
        "src/engine/init.asm",
        "src/engine/utils.asm",
        "src/engine/initsnes.asm",
        "src/engine/crashhandler.asm",
        "src/engine/gameloop.asm",
        "src/engine/audio.asm",
        "src/engine/dma.asm",
        "src/engine/graphics.asm",
        "src/engine/dmamanager.asm",
        "src/engine/gameobjects.asm",
        
        -- Game files
        "examples/walker-dynamic/header.asm",
        "examples/walker-dynamic/main.asm",
        "examples/walker-dynamic/walkerscene.asm",
        "examples/walker-dynamic/dragon.asm",
    },
    target = "superfeather-ex-walker-dynamic",
    linkerConfig = "examples/config-ca65.cfg"
}
