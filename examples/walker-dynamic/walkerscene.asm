
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/dma.inc"
.include "../../inc/engine/graphics.inc"

.import SetupPpuRegisters:far, BeginSprites, UpdateObjects, FinalizeSprites, sceneThinkFunc, screenBrightness, spriteScrollX, spriteScrollY, bg1ScrollX, bg1ScrollY

.import SpawnDragon

.export WalkerSceneInit, WalkerSceneThink

.segment "DATA2"

DragonPal:
    .INCBIN "examples/walker-dynamic/data/dragon.pal"
DragonPal_Len = * - DragonPal

FloorChar:
    .INCBIN "examples/walker-dynamic/data/floor.pic"
FloorChar_Len = * - FloorChar

FloorMap:
    .INCBIN "examples/walker-dynamic/data/floor.map"
FloorMap_Len = * - FloorMap


.segment "CODE0"

;                            OBJSEL,                                BGMODE,    BG1TILE,                                      BG2TILE,                                      BG3TILE,                                      BG4TILE,   BG12CHAR,                                      BG34CHAR,                                      MODE7SEL,  WIN12SEL,  WIN34SEL,  WINOBJSEL, WINBGLOG,  WINOBJLOG, DESTMAIN,  DESTSUB,   WINMAIN,   WINSUB,    CMATHSEL,  CMATHDEST, FIXEDCOLOR,        SETINI
Define_PpuDef SceneInitRegs, PPU_OBJSIZE_16_32 | PPU_OBJCHARADDR_0, %00001001, PPU_TILEMAPSIZE_64_64 | PPU_TILEMAPADDR_6000, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_7000, PPU_TILEMAPSIZE_32_32 | PPU_TILEMAPADDR_7800, %00000000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, PPU_BG13CHARADDR_2000 | PPU_BG24CHARADDR_2000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00011111, %00000000, %00000000, %00000000, %00000000, %00000000, %0000000000000000, %00000000


; ============================================================================
; Initializes our scene by loading the background and sprite data to CGRAM/VRAM,
;  and then spawning our dragon.
; ============================================================================
WalkerSceneInit:
    php
    
    rep #$10
    sep #$20
    
    ; Initialize the registers using the data structure above.
    ldx #.loword(SceneInitRegs)
    jsl SetupPpuRegisters

    ; Load palettes, VRAM data, etc. here.
    Macro_LoadPalette DragonPal, #0, #DragonPal_Len
    ; Dragon char data is not loaded upfront this time.
    
    Macro_LoadVRAM FloorChar, #$2000, #FloorChar_Len
    Macro_LoadVRAM FloorMap, #$6000, #FloorMap_Len
    
    ; Create our dragon friend
    jsr SpawnDragon
    
    plp
    rts


; ============================================================================
; The thinker function for this scene, called every frame.
; ============================================================================
WalkerSceneThink:
    php
    
    sep #$20
    lda #%00001111
    sta screenBrightness
    rep #$30
    
    jsr BeginSprites
    jsr UpdateObjects
    jsr FinalizeSprites
    
    ; Synchronize BG layer with sprite scroll
    lda spriteScrollX
    sta bg1ScrollX
    lda spriteScrollY
    sta bg1ScrollY
    
    plp
    rts
