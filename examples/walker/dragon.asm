
.p816   ; 65816 processor
.a16
.i16
.smart

.include "sounds.inc"
.include "../../inc/engine/constants.inc"
.include "../../inc/engine/gameobjects.inc"
.include "../../inc/engine/graphics.inc"

.export SpawnDragon, DragonChar, DragonChar_Len

.import CreateObject, MoveObject, SetAnimation, AnimateWithCallback, AddSimpleSprite
.import inputRaw, objThinker, objPosX, objPosY, objPosSubX, objPosSubY, objVelX, objVelY, objAttribute, objAnimDef, objCustomA, objCustomB, spriteScrollX, spriteScrollY, soundAId

.segment "CODE0"

DragonChar:
    .INCBIN "examples/walker/data/dragon.pic"
DragonChar_Len = * - DragonChar


; ============================================================================
; Individual hardware sprites to display are listed below
; ============================================================================
;                     Name,             X pos, Y pos, sprite size,  tile, vhoopppN, next sprite
Define_SpriteDef      Dragon_S1,        $fff0, $fff0, SPRITE_LARGE, $00, %00100000, 0
Define_SpriteDef      Dragon_S2,        $fff0, $ffef, SPRITE_LARGE, $04, %00100000, 0
Define_SpriteDef      Dragon_S3,        $fff0, $ffee, SPRITE_LARGE, $04, %00100000, 0
Define_SpriteDef      Dragon_S4,        $fff0, $fff0, SPRITE_LARGE, $00, %01100000, 0
Define_SpriteDef      Dragon_S5,        $fff0, $ffef, SPRITE_LARGE, $04, %01100000, 0
Define_SpriteDef      Dragon_S6,        $fff0, $ffee, SPRITE_LARGE, $04, %01100000, 0

Define_SpriteDef      Dragon_SE1,       $fff0, $fff0, SPRITE_LARGE, $40, %00100000, 0
Define_SpriteDef      Dragon_SE2,       $fff0, $ffef, SPRITE_LARGE, $44, %00100000, 0
Define_SpriteDef      Dragon_SE3,       $fff0, $ffee, SPRITE_LARGE, $44, %00100000, 0
Define_SpriteDef      Dragon_SE4,       $fff0, $fff0, SPRITE_LARGE, $40, %00100000, 0
Define_SpriteDef      Dragon_SE5,       $fff0, $ffef, SPRITE_LARGE, $48, %00100000, 0
Define_SpriteDef      Dragon_SE6,       $fff0, $ffee, SPRITE_LARGE, $48, %00100000, 0

Define_SpriteDef      Dragon_E1,        $ffea, $fff0, SPRITE_LARGE, $80, %00100000, 0
Define_SpriteDef      Dragon_E2,        $ffea, $ffef, SPRITE_LARGE, $84, %00100000, 0
Define_SpriteDef      Dragon_E3,        $ffea, $ffee, SPRITE_LARGE, $84, %00100000, 0
Define_SpriteDef      Dragon_E4,        $ffea, $fff0, SPRITE_LARGE, $80, %00100000, 0
Define_SpriteDef      Dragon_E5,        $ffea, $ffef, SPRITE_LARGE, $88, %00100000, 0
Define_SpriteDef      Dragon_E6,        $ffea, $ffee, SPRITE_LARGE, $88, %00100000, 0

Define_SpriteDef      Dragon_NE1,       $fff0, $fff0, SPRITE_LARGE, $c0, %00100000, 0
Define_SpriteDef      Dragon_NE2,       $fff0, $ffef, SPRITE_LARGE, $c4, %00100000, 0
Define_SpriteDef      Dragon_NE3,       $fff0, $ffee, SPRITE_LARGE, $c4, %00100000, 0
Define_SpriteDef      Dragon_NE4,       $fff0, $fff0, SPRITE_LARGE, $c0, %00100000, 0
Define_SpriteDef      Dragon_NE5,       $fff0, $ffef, SPRITE_LARGE, $c8, %00100000, 0
Define_SpriteDef      Dragon_NE6,       $fff0, $ffee, SPRITE_LARGE, $c8, %00100000, 0

Define_SpriteDef      Dragon_N1,        $fff0, $fff0, SPRITE_LARGE, $08, %00100000, 0
Define_SpriteDef      Dragon_N2,        $fff0, $ffef, SPRITE_LARGE, $0c, %00100000, 0
Define_SpriteDef      Dragon_N3,        $fff0, $ffee, SPRITE_LARGE, $0c, %00100000, 0
Define_SpriteDef      Dragon_N4,        $fff0, $fff0, SPRITE_LARGE, $08, %01100000, 0
Define_SpriteDef      Dragon_N5,        $fff0, $ffef, SPRITE_LARGE, $0c, %01100000, 0
Define_SpriteDef      Dragon_N6,        $fff0, $ffee, SPRITE_LARGE, $0c, %01100000, 0

Define_SpriteDef      Dragon_NW1,       $fff0, $fff0, SPRITE_LARGE, $c0, %01100000, 0
Define_SpriteDef      Dragon_NW2,       $fff0, $ffef, SPRITE_LARGE, $c4, %01100000, 0
Define_SpriteDef      Dragon_NW3,       $fff0, $ffee, SPRITE_LARGE, $c4, %01100000, 0
Define_SpriteDef      Dragon_NW4,       $fff0, $fff0, SPRITE_LARGE, $c0, %01100000, 0
Define_SpriteDef      Dragon_NW5,       $fff0, $ffef, SPRITE_LARGE, $c8, %01100000, 0
Define_SpriteDef      Dragon_NW6,       $fff0, $ffee, SPRITE_LARGE, $c8, %01100000, 0

Define_SpriteDef      Dragon_W1,        $fff6, $fff0, SPRITE_LARGE, $80, %01100000, 0
Define_SpriteDef      Dragon_W2,        $fff6, $ffef, SPRITE_LARGE, $84, %01100000, 0
Define_SpriteDef      Dragon_W3,        $fff6, $ffee, SPRITE_LARGE, $84, %01100000, 0
Define_SpriteDef      Dragon_W4,        $fff6, $fff0, SPRITE_LARGE, $80, %01100000, 0
Define_SpriteDef      Dragon_W5,        $fff6, $ffef, SPRITE_LARGE, $88, %01100000, 0
Define_SpriteDef      Dragon_W6,        $fff6, $ffee, SPRITE_LARGE, $88, %01100000, 0

Define_SpriteDef      Dragon_SW1,       $fff0, $fff0, SPRITE_LARGE, $40, %01100000, 0
Define_SpriteDef      Dragon_SW2,       $fff0, $ffef, SPRITE_LARGE, $44, %01100000, 0
Define_SpriteDef      Dragon_SW3,       $fff0, $ffee, SPRITE_LARGE, $44, %01100000, 0
Define_SpriteDef      Dragon_SW4,       $fff0, $fff0, SPRITE_LARGE, $40, %01100000, 0
Define_SpriteDef      Dragon_SW5,       $fff0, $ffef, SPRITE_LARGE, $48, %01100000, 0
Define_SpriteDef      Dragon_SW6,       $fff0, $ffee, SPRITE_LARGE, $48, %01100000, 0


; ============================================================================
; Animation definitions are below.
; After each animdef, we have an array of sprites to display for each frame,
;  based on which direction the dragon is facing. See the lookup tables below.
; ============================================================================
;                     Name,                   delay, next anim,       callback
Define_AnimDef        Dragon_AnimIdle,        0,     Dragon_AnimIdle, 0
.word .loword(Dragon_S1)
.word .loword(Dragon_SE1)
.word .loword(Dragon_E1)
.word .loword(Dragon_NE1)
.word .loword(Dragon_N1)
.word .loword(Dragon_NW1)
.word .loword(Dragon_W1)
.word .loword(Dragon_SW1)

Define_AnimDef        Dragon_AnimWalk_1,      7,     Dragon_AnimWalk_2,   DragonFootstep1
.word .loword(Dragon_S1)
.word .loword(Dragon_SE1)
.word .loword(Dragon_E1)
.word .loword(Dragon_NE1)
.word .loword(Dragon_N1)
.word .loword(Dragon_NW1)
.word .loword(Dragon_W1)
.word .loword(Dragon_SW1)

Define_AnimDef        Dragon_AnimWalk_2,      5,     Dragon_AnimWalk_3,   0
.word .loword(Dragon_S2)
.word .loword(Dragon_SE2)
.word .loword(Dragon_E2)
.word .loword(Dragon_NE2)
.word .loword(Dragon_N2)
.word .loword(Dragon_NW2)
.word .loword(Dragon_W2)
.word .loword(Dragon_SW2)

Define_AnimDef        Dragon_AnimWalk_3,      5,     Dragon_AnimWalk_4,   0
.word .loword(Dragon_S3)
.word .loword(Dragon_SE3)
.word .loword(Dragon_E3)
.word .loword(Dragon_NE3)
.word .loword(Dragon_N3)
.word .loword(Dragon_NW3)
.word .loword(Dragon_W3)
.word .loword(Dragon_SW3)

Define_AnimDef        Dragon_AnimWalk_4,      7,     Dragon_AnimWalk_5,   DragonFootstep2
.word .loword(Dragon_S4)
.word .loword(Dragon_SE4)
.word .loword(Dragon_E4)
.word .loword(Dragon_NE4)
.word .loword(Dragon_N4)
.word .loword(Dragon_NW4)
.word .loword(Dragon_W4)
.word .loword(Dragon_SW4)

Define_AnimDef        Dragon_AnimWalk_5,      5,     Dragon_AnimWalk_6,   0
.word .loword(Dragon_S5)
.word .loword(Dragon_SE5)
.word .loword(Dragon_E5)
.word .loword(Dragon_NE5)
.word .loword(Dragon_N5)
.word .loword(Dragon_NW5)
.word .loword(Dragon_W5)
.word .loword(Dragon_SW5)

Define_AnimDef        Dragon_AnimWalk_6,      5,     Dragon_AnimWalk_1,   0
.word .loword(Dragon_S6)
.word .loword(Dragon_SE6)
.word .loword(Dragon_E6)
.word .loword(Dragon_NE6)
.word .loword(Dragon_N6)
.word .loword(Dragon_NW6)
.word .loword(Dragon_W6)
.word .loword(Dragon_SW6)


; ============================================================================
; Converts 16-bit D-pad value to direction index. ffff = invalid input, or no input
; 0   2   4   6   8   a   c   e
; S,  SE, E,  NE, N,  NW, W,  SW
; ============================================================================
_DpadToDirectionValue:  ; up | down | left | right
    .word $ffff         ;  0 |    0 |    0 |     0
    .word $0004         ;  0 |    0 |    0 |     1
    .word $000c         ;  0 |    0 |    1 |     0
    .word $ffff         ;  0 |    0 |    1 |     1
    .word $0000         ;  0 |    1 |    0 |     0
    .word $0002         ;  0 |    1 |    0 |     1
    .word $000e         ;  0 |    1 |    1 |     0
    .word $ffff         ;  0 |    1 |    1 |     1
    .word $0008         ;  1 |    0 |    0 |     0
    .word $0006         ;  1 |    0 |    0 |     1
    .word $000a         ;  1 |    0 |    1 |     0
    .word $ffff         ;  1 |    0 |    1 |     1
    .word $ffff         ;  1 |    1 |    0 |     0
    .word $ffff         ;  1 |    1 |    0 |     1
    .word $ffff         ;  1 |    1 |    1 |     0
    .word $ffff         ;  1 |    1 |    1 |     1


; ============================================================================
; Speed constants
; ============================================================================
WALK_SPEED = $200
DIAG_SPEED = $16a


; ============================================================================
; Converts direction index to X and Y velocities
; ============================================================================
_DirectionToVelocity:
            ; X vel,               Y vel,                direction
    .word     0,                   WALK_SPEED          ; S
    .word     DIAG_SPEED,          DIAG_SPEED          ; SE
    .word     WALK_SPEED,          0                   ; E
    .word     DIAG_SPEED,          $10000 - DIAG_SPEED ; NE
    .word     0,                   $10000 - WALK_SPEED ; N
    .word     $10000 - DIAG_SPEED, $10000 - DIAG_SPEED ; NW
    .word     $10000 - WALK_SPEED, 0                   ; W
    .word     $10000 - DIAG_SPEED, DIAG_SPEED          ; SW


; ============================================================================
; Our variable aliases
; ============================================================================
dragonDirection = objCustomA
lastAnimation = objCustomB


; ============================================================================
; These callbacks play footstep noises as the dragon walks.
; ============================================================================
DragonFootstep1:
    lda #SOUND_STEP_1
    sta soundAId
    rts

DragonFootstep2:
    lda #SOUND_STEP_2
    sta soundAId
    rts


; ============================================================================
; Creates the dragon object and initializes the properties
; ============================================================================
SpawnDragon:
    php
    rep #$30
    
    ; Use object pool 0
    ldy #$0000
    ; Try to get an object index from the pool
    jsr CreateObject
    ; If overflow CPU bit is set, we ran out of objects.
    bvs _NotCreated
    ; Otherwise, our X is set to the new object's index. Let's set some variables.
    
    ; Initial position
    sep #$20
    stz objPosSubX, x
    stz objPosSubY, x
    rep #$20
    lda #$0080
    sta objPosX, x
    lda #$0070
    sta objPosY, x
    
    ; Velocity, sprite attribute
    stz objVelX, x
    stz objVelY, x
    stz objAttribute, x
    
    ; Give this object a behavior to run every frame
    lda #.loword(DragonThink)
    sta objThinker, x
    
    ; Give this object an animation.
    ldy #.loword(Dragon_AnimIdle)
    jsr SetAnimation
    
_NotCreated:
    plp
    rts


; ============================================================================
; Thinker function for our dragon. Handles movement and animation.
; ============================================================================
DragonThink:
    ; Get the current directional input
    lda inputRaw
    ; We're only interested in the directions
    and #( INPUT_UP + INPUT_DOWN + INPUT_LEFT + INPUT_RIGHT)
    ; All the directional inputs are in the high byte, so switch to the low byte
    xba
    ; Convert to direction index. Shift left one byte for word-sized index (two bytes).
    asl
    tay
    lda _DpadToDirectionValue, y
    ; If no valid direction, don't change it.
    bmi _NoDirection
    sta dragonDirection, x
    
    ; Since we're holding a direction, set our velocity
    
    ; 4 bytes per table index, so shift left
    asl
    tay
    ; Read X and Y values from the table
    lda _DirectionToVelocity, y
    sta objVelX, x
    lda _DirectionToVelocity + 2, y
    sta objVelY, x
    
    ; Now see if we need to make our dragon walk. Custom value B is the last
    ;  animation we switched to.
    lda lastAnimation, x
    cmp #.loword(Dragon_AnimWalk_1)
    beq _VelocitySet
    
    ; Play the animation
    ldy #.loword(Dragon_AnimWalk_1)
    jsr SetAnimation
    tya
    sta lastAnimation, x
    
    bra _VelocitySet
    
_NoDirection:
    ; Stop in place
    stz objVelX, x
    stz objVelY, x
    
    ; Now see if we need to make our dragon switch to the idle animation.
    lda lastAnimation, x
    cmp #.loword(Dragon_AnimIdle)
    beq _VelocitySet
    
    ; Play the animation
    ldy #.loword(Dragon_AnimIdle)
    jsr SetAnimation
    tya
    sta lastAnimation, x

_VelocitySet:
    
    jsr MoveObject
    
    ; Scroll the view if necessary (only if we happen to be object 0)
    cpx #$0000
    bne _NoScroll
    jsr _ScrollView

_NoScroll:
    
    ; Process any animation
    jsr AnimateWithCallback
    ; Our sprite is at the end of the current animdef. Find it and put it in Y.
    lda objAnimDef, x
    ; The end of each animation def has an array of sprites to use based on which
    ;  direction we're facing. So let's offset into that array here.
    clc
    adc dragonDirection
    tay
    ; Here we get the actual spritedef out of the array
    lda ANIMDEF_DATAEX, y
    tay
    jsr AddSimpleSprite
    
    ; Go to next object (well there's only one object in this example, so...)
    Macro_NextThinker


; ============================================================================
; Centers the view on our dragon where appropriate
; ============================================================================
_ScrollView:
    ; Where we would like to move the scroll X position to, if going left.
    ;  Keep in mind that spriteScrollX refers to where the left edge of the screen is!
    lda objPosX, x
    sec
    sbc #$70
    tay
    
    ; If target scroll is to the left of current scroll, then let's scroll there
    sec
    sbc spriteScrollX
    bpl _NoLeftScroll
    sty spriteScrollX
    
    bra _DoYScroll
    
_NoLeftScroll:
    ; Target X scroll, if going right
    lda objPosX, x
    sec
    sbc #$90
    tay
    
    ; If target scroll is to the right of current scroll...
    sec
    sbc spriteScrollX
    bmi _NoRightScroll
    sty spriteScrollX

_NoRightScroll:
_DoYScroll:
    ; Target Y scroll, if going up
    ;  Keep in mind that spriteScrollY refers to where the top edge of the screen is!
    lda objPosY, x
    sec
    sbc #$60
    tay
    
    ; If target scroll is above current scroll...
    sec
    sbc spriteScrollY
    bpl _NoTopScroll
    sty spriteScrollY
    
    bra _ScrollDone
    
_NoTopScroll:
    ; Target Y scroll, if going down
    lda objPosY, x
    sec
    sbc #$80
    tay
    
    ; If target scroll is below of current scroll...
    sec
    sbc spriteScrollY
    bmi _NoBottomScroll
    sty spriteScrollY

_NoBottomScroll:
_ScrollDone:
    
    rts
