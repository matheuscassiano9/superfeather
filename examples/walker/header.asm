
.p816   ; 65816 processor
.smart

.import Reset, VBlank, CrashHandler

.segment "HEADER"
    .byte 0, 0 ; Maker code
    .byte "SNES" ; Game code
    .byte 0, 0, 0, 0, 0, 0, 0 ; Fixed value
    .byte 0 ; No expansion RAM
    .byte 0, 0 ; Special version, cartridge type
    
    ;      123456789012345678901
    .byte "WALKER EXAMPLE       " ; ROM name, 21 bytes
    .byte $31            ; HiROM, fast-capable
    .byte 0              ; ROM-only
    .byte $08            ; 256K ROM
    .byte 0              ; No SRAM
    .byte $01            ; Country code
    .byte $33            ; Fixed value
    .byte 0              ; Version number
    .word $FFFF, $0000   ; Complement and checksum (will be set later)
    
    ; Vectors
    
    ; Native
    .word $ffff, $ffff ; Unused
    .word .loword(EmptyVector) ; Coprocessor
    .word .loword(CrashHandler) ; Break
    .word .loword(EmptyVector) ; Abort
    .word .loword(VBlank) ; NMI
    .word .loword(EmptyVector) ; Reset
    .word .loword(EmptyVector) ; IRQ
    ; Emulation
    .word $ffff, $ffff ; Unused
    .word .loword(EmptyVector) ; Coprocessor
    .word .loword(CrashHandler) ; Break
    .word .loword(EmptyVector) ; Abort
    .word .loword(EmptyVector) ; NMI
    .word .loword(Reset) ; Reset
    .word .loword(EmptyVector) ; IRQ

.segment "CODE0"
    EmptyVector:
        rts
