
.p816   ; 65816 processor
.smart

.import SpcDriver, SpcUpload, SpcSendCommand
.import spcCodeStart, spcUploadSize, spcUploadDest, spcUploadSrc

; ----
; Expects any, clobbers A
; ----
.macro Macro_SpcUpload SOURCE, DESTINATION, CODESTART
    php
    sep #$20
    phb
    lda #<.bank(SOURCE)
    pha
    plb

    rep #$20
    lda #.loword(SOURCE+2)
    sta f:spcUploadSrc
    lda DESTINATION
    sta f:spcUploadDest
    lda .loword(SOURCE)
    sta f:spcUploadSize
    lda CODESTART
    sta f:spcCodeStart
    jsr SpcUpload
    
    sep #$20
    plb
    plp
.endmacro

; ----
; Expects any, clobbers A
; ----
.macro Macro_SpcUploadMusic SOURCE
    php
    sep #$20
    phb
    lda #<.bank(SOURCE)
    pha
    plb

    rep #$20
    lda #.loword(SOURCE+2)
    sta f:spcUploadSrc
    lda f:SpcDriver
    clc
    adc #$200
    sta f:spcUploadDest
    lda .loword(SOURCE)
    sta f:spcUploadSize
    lda #$200
    sta f:spcCodeStart
    jsr SpcUpload
    
    sep #$20
    plb
    plp
.endmacro

; ----
; Expects any, clobbers X, Y
; ----
.macro Macro_SpcCommand COMMAND, PARAMETERS
    php
    rep #$10
    ldx COMMAND
    ldy PARAMETERS
    jsr SpcSendCommand
    plp
.endmacro
