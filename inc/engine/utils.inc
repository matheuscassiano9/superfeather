
.p816   ; 65816 processor
.smart

.macro Define_BoundingBoxDef NAME, LEFT, TOP, WIDTH, HEIGHT, DATAPOINTER
    NAME:
    .word LEFT
    .word TOP
    .word WIDTH
    .word HEIGHT
    .word .loword(DATAPOINTER)
.endmacro
