
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/crashhandler.inc"

.import SpcDriver
.export SpcUpload, SpcSendCommand, SpcBusyHeartbeat, SpcUpdateSounds
.export spcUploadSrc, spcUploadDest, spcUploadSize, spcCodeStart, soundAId, soundAPan, soundAVolume, soundBId, soundBPan, soundBVolume

.segment "LORAM"

spcUploadSrc: .res 2
spcUploadDest: .res 2
spcUploadSize: .res 2
spcCodeStart: .res 2
spcBusy: .res 2

soundAId: .res 2 ; Only one byte, reserves two for convenience. Don't write non-zero to high byte!
soundAPan: .res 1
soundAVolume: .res 1
soundBId: .res 2 ; Same here.
soundBPan: .res 1
soundBVolume: .res 1

.segment "DATA0"

ErrorSpcTimeout:
    .byte "Audio driver has a sleep!       SPC communication timed out..."

.segment "CODE0"


; ============================================================================
; Expects any, clobbers A, X, Y
; ============================================================================
SpcUpdateSounds:
    php
    rep #$30
    
    ; Check for sound A
    
    lda soundAId
    beq _NoSoundA
    
    ora soundAPan - 1
    tay
    
    lda soundAVolume
    and #$ff
    xba
    ora #$69
    tax
    jsr SpcSendCommand

_NoSoundA:
    stz soundAId
    lda #$ff80
    sta soundAPan
    
    ; Check for sound B

    lda soundBId
    beq _NoSoundB
    
    ora soundBPan - 1
    tay
    
    lda soundBVolume
    and #$ff
    xba
    ora #$79
    tax
    jsr SpcSendCommand

_NoSoundB:
    stz soundBId
    lda #$ff80
    sta soundBPan
    
    plp
    rts


; ============================================================================
; Expects any, clobbers nothing
; ============================================================================
SpcUpload:
    php
    
    rep #$30
    pha
    phx
    phy
    phd
    
    lda #$2100
    tcd
    
    lda f:spcBusy
    inc a
    sta f:spcBusy
    
_WaitReady:
    lda $40
    cmp #$bbaa ; Look for ready signal
    bne _WaitReady
    
    lda #$0001
    sep #$20
    
    sta $41
    lda f:spcUploadDest
    sta $42 ; Destination address, low byte
    lda f:spcUploadDest+1
    sta $43 ; Destination address, high byte
    
    lda #$cc
    sta $40
    
_WaitTransferReady:
    lda $40
    cmp #$cc ; Look for ready signal
    bne _WaitTransferReady
    
    ldx #$00
    rep #$20
    lda f:spcUploadSrc
    tay
    
_TransferByte:
    sep #$20
    lda a:0, y
    sta $41
    txa
    sta $40
    
_WaitGotByte:
    cmp $40
    bne _WaitGotByte
    
    inx
    iny
    rep #$20
    txa
    cmp f:spcUploadSize
    bne _TransferByte
    
    ; Finish transfer
    sep #$20
    stz $41
    lda f:spcCodeStart
    sta $42 ; Code address, low byte
    lda f:spcCodeStart+1
    sta $43 ; Code address, high byte
    
    lda $40
    inc a
    inc a
    sta $40
    
    rep #$30
    
    lda #$0000
    sta f:spcBusy
    
    pld
    ply
    plx
    pla
    
    plp
    rts


; ============================================================================
; Expects any, clobbers A
; ============================================================================
SpcSendCommand:
    php
    rep #$20
    pha
    sep #$20

    lda f:spcBusy
    inc a
    sta f:spcBusy
_WaitCommandReady:
    lda $2140
    bne _WaitCommandReady

    rep #$30

    sty $2142
    stx $2140
    
    sep #$20
    
    cpx #SPC_CMD_LOAD
    beq _SkipWaitAck

_WaitCommandAck:
    lda $2140
    beq _WaitCommandAck
    
_SkipWaitAck:
    rep #$20
    
    lda #$0000
    sta f:spcBusy
    
    pla
    plp
    rts

SpcBusyHeartbeat:
    php
    rep #$30
    
    lda f:spcBusy
    beq _SpcIdle
    inc a
    cmp #$100
    bpl _SpcTimeout
    sta f:spcBusy
_SpcIdle:
    
    plp
    rts
    
_SpcTimeout:
    Macro_FatalError #.loword(ErrorSpcTimeout), spcBusy, spcBusy, spcBusy
