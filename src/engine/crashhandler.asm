
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/dma.inc"

.import PrintHexToTilemap, PrintHexByteToTilemap, frameReady
.export CrashHandler

.macro _Macro_SetDataBank BANK
    sep #$20
    lda #BANK
    pha
    plb
.endmacro

_CRASH_STACK_A = 1
_CRASH_STACK_X = 3
_CRASH_STACK_Y = 5
_CRASH_STACK_D = 7
_CRASH_STACK_DB = 9
_CRASH_STACK_P = 10
_CRASH_STACK_PB = 13
_CRASH_STACK_PC = 11
_ERROR_STACK_ARG3 = 14
_ERROR_STACK_ARG2 = 16
_ERROR_STACK_ARG1 = 18
_ERROR_STACK_MESSAGE = 20
_ERROR_STACK_MAGIC2 = 22
_ERROR_STACK_MAGIC1 = 24


.macro _WriteStackWord VARTOWRITE, DEST
    lda VARTOWRITE, s
    tax
    ldy #.loword(hudCrashTilemap) + DEST
    jsr PrintHexToTilemap
.endmacro

.macro _WriteStackByte VARTOWRITE, DEST
    lda VARTOWRITE, s
    and #$ff
    tax
    ldy #.loword(hudCrashTilemap) + DEST
    jsr PrintHexByteToTilemap
.endmacro

.segment "EXRAM"

tempA: .res 2
hudCrashTilemap: .res $100

.segment "DATA0"

CrashTilemap:
    .incbin "data/crashhandler/crashtilemap.map"

CustomErrorTilemap:
    .incbin "data/crashhandler/customerrortilemap.map"

CrashChar:
    .incbin "data/crashhandler/crashchar.pic2"

CrashPal:
    .incbin "data/crashhandler/crashchar.pal"

.segment "CODE0"


; ============================================================================
; Crash handler entry point for BRK vector
; ============================================================================
CrashHandler:
    jml CrashHandlerInit

.segment "DATA0"

CrashHandlerInit:
    rep #$30
    ; Handle erroneous stack pointer
    sta tempA
    tsc
    cmp #$2000
    bcc _StackPointerGood
    lda #$1ffd
    tcs
    
_StackPointerGood:
    lda tempA
    
    phb
    phd
    phy
    phx
    pha
    
    sep #$20
    ; Go to bank 80 so we can access registers and such
    lda #$80
    pha
    plb
    
    stz frameReady
    
    cli
    ; Set interrupts
    ;     n yx   a
    lda #%10000001
    sta $4200
    
    ; Wait for no HDMA before the WRAM DMA
    wai
    
    rep #$30
    
    ; Check what kind of error (via magic number).
    ; _Try_ to avoid hitting custom error due to undefined behavior
    lda _ERROR_STACK_MAGIC1, s
    tax
    cpx #$debb
    bne _CrashBrkHandler
    lda _ERROR_STACK_MAGIC2, s
    tax
    cpx #$f1ff
    bne _CrashBrkHandler
    
    sep #$20

    ; Setup error info tilemap
    Macro_LoadWRAM CustomErrorTilemap, hudCrashTilemap, #$100

    _Macro_SetDataBank <.bank(hudCrashTilemap)
    
    ldy #$00
    
_ErrorPrintLoop:
    rep #$20
    
    ; Add y index (divided by 2) to start of message to get char pointer
    tya
    lsr
    clc
    adc _ERROR_STACK_MESSAGE, s
    tax
    
    ; Load letter from char pointer...
    lda $c00000, x
    and #$7f
    tax
    
    ; ...And translate from ASCII to tile ID
    lda f:AsciiToCrashChar, x
    sep #$20
    sta hudCrashTilemap + $42, y
    
    iny
    iny
    cpy #$7c
    bcc _ErrorPrintLoop
    
    rep #$30
    
    _WriteStackWord _ERROR_STACK_ARG1, $dc
    _WriteStackWord _ERROR_STACK_ARG2, $e8
    _WriteStackWord _ERROR_STACK_ARG3, $f4
    
    bra _ErrorPrintDone
    
_CrashBrkHandler:
    sep #$20

    ; Setup error info tilemap
    Macro_LoadWRAM CrashTilemap, hudCrashTilemap, #$100
    
    _Macro_SetDataBank <.bank(hudCrashTilemap)
    
    rep #$30
    
    _WriteStackWord _CRASH_STACK_A, $c2 ; A
    _WriteStackWord _CRASH_STACK_X, $cc ; X
    _WriteStackWord _CRASH_STACK_Y, $d6 ; Y
    _WriteStackWord _CRASH_STACK_D, $e2 ; D
    _WriteStackByte _CRASH_STACK_DB, $ee ; DB
    _WriteStackByte _CRASH_STACK_P, $f8 ; P flags
    _WriteStackByte _CRASH_STACK_PB, $72 ; PB
    _WriteStackWord _CRASH_STACK_PC, $76 ; PC

_ErrorPrintDone:
    
    ; Setup video registers
    wai
    
    sep #$20
    
    _Macro_SetDataBank $80
    
    ; Mode 1, with BG3 HUD
    lda #%00001001
    sta $2105
    
    ; BG3 tilemap pos and size
    lda #%11111010
    sta $2109
    
    ; BG3 char address
    lda #%00100010
    sta $210c
    
    ; Reset scroll
    stz $2111
    stz $2111
    lda #$ff
    sta $2112
    sta $2112
    
    ; Screen pixelation
    stz $2106
    
    ; Main screen enable - Enable BG3 only
    ;        s4321
    lda #%00000100
    sta $212c
    
    ; Send crash info tiles to VRAM
    Macro_LoadPalette CrashPal, #0, #32
    Macro_LoadVRAM CrashChar, #$2000, #$0400
    
    lda #$80
    sta $2115        ; Set VRAM transfer mode to word-access, increment by 1

    ldx #$7800       ; DEST
    stx $2116        ; $2116: Word address for accessing VRAM.
    lda #<.bank(hudCrashTilemap)  ; SRCBANK
    ldx #.loword(hudCrashTilemap) ; SRCOFFSET
    ldy #$100        ; SIZE
    jsr LoadVRAM
    
    ; Turn on screen (just in case)
    lda #%00001111
    sta $2100
    
    sep #$30

CrashHandlerLoop:
    wai
    jmp CrashHandlerLoop

AsciiToCrashChar:
    .byte  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
    .byte  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
    .byte  0,  40, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  39, 0,  38, 0
    .byte  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 42, 0,  43, 0,  44, 41
    .byte  0,  12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26
    .byte  27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 0,  0,  0,  0,  0
    .byte  0,  12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26
    .byte  27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 0,  0,  0,  0,  0
