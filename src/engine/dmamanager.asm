; DMA VRAM manager

.p816   ; 65816 processor
.a16
.i16
.smart

.export DMAInitialize, DMAPrepare, DMATryAdd, DMATryAddStrip4, DMATryQueueAdd, DMATryQueueAddStrip4, DMADoTransfer, VramAllocateSlot, VramDeallocateSlot
.export dmaTableSrc, dmaTableStripSrc, dmaTableSrcBank, dmaTableStripSrcBank, dmaTableDest, dmaTableStripDest, dmaTableSize, dmaTableStripSize, dmaTableIndex, dmaTableStripIndex, dmaTotalSize, dmaQueueMaxSize

; The following overhead constants assume FastROM!
DMA_OVERHEAD = $2b ; Timing found using bsnes-plus (42.25 slow cycles, rounded up)
DMA_OVERHEAD_STRIP4 = $84 ; Also found using bsnes-plus
DMA_TOTAL_SIZE_LIMIT = $1400 ; Currently a guess

DMA_TABLE_MAIN_SIZE = 64 ; 2-byte index
DMA_TABLE_STRIP_SIZE = 32 ; 2-byte index
DMA_QUEUE_SIZE = 64 ; Must be a power of 2!! 2-byte index
DMA_QUEUE_MASK = DMA_QUEUE_SIZE - 1

QUEUE_PRIORITY_NONE = 0 ; Must be zero
QUEUE_PRIORITY_QUEUED = 1
QUEUE_PRIORITY_HIGH_UNSENT = 2
QUEUE_PRIORITY_HIGH_DELAYED = 3

VRAM_SLOT_COUNT = 8 ; Multiple of 1, no larger than DMA_QUEUE_SIZE / 8. Also, values larger than 8 aren't useful
VRAM_TOTAL_SLOT_COUNT = 32

.segment "LORAM"

dmaTableSrc: .res DMA_TABLE_MAIN_SIZE ; 2-byte index
dmaTableSrcBank: .res DMA_TABLE_MAIN_SIZE ; 2-byte index
dmaTableDest: .res DMA_TABLE_MAIN_SIZE ; 2-byte index
dmaTableSize: .res DMA_TABLE_MAIN_SIZE ; 2-byte index
dmaTableIndex: .res 2 ; 2-byte index

dmaTableStripSrc: .res DMA_TABLE_STRIP_SIZE ; 2-byte index
dmaTableStripSrcBank: .res DMA_TABLE_STRIP_SIZE ; 2-byte index
dmaTableStripDest: .res DMA_TABLE_STRIP_SIZE ; 2-byte index
dmaTableStripSize: .res DMA_TABLE_STRIP_SIZE ; 2-byte index
dmaTableStripIndex: .res 2 ; 2-byte index

dmaTotalSize: .res 2

dmaQueueItems: .res DMA_QUEUE_SIZE ; 2-byte index
dmaQueueItemSize: .res DMA_QUEUE_SIZE ; 2-byte index
dmaQueuePriority: .res DMA_QUEUE_SIZE ; 2-byte index

dmaQueueTotalSize: .res 2
dmaQueueMaxSize: .res 2
dmaQueueHead: .res 2 ; 2-byte index
dmaQueueTailLength: .res 2 ; Note: 1-byte index for performance reasons
dmaQueuePoppedNum: .res 2 ; Also 1-byte index

_dmaScratch: .res 2
_dmaNewTotalSize:
_dmaScratch2:
.res 2
_slotCandidate: .res 1

; This is byte-size to conserve LORAM
vramSlotAllocated: .res VRAM_SLOT_COUNT

.segment "HIRAM"

vramSlotAllocatedDiff: .res VRAM_TOTAL_SLOT_COUNT

.segment "CODE0"


; ============================================================================
; DMAInitialize - Called at the beginning of the scene.
; ---------------------------------------------------------------------------
; Expects anything
; ============================================================================
DMAInitialize:
    php
    rep #$30
    
    ; Default DMA queue size
    lda #DMA_TOTAL_SIZE_LIMIT
    sta dmaQueueMaxSize
    
    ; Initialize the allocation size fields
    sep #$20
    lda #$ff
    ldx #$0000
    
_InitAllocSizeLoop:
    sta f:vramSlotAllocatedDiff, x
    inx
    cpx #VRAM_TOTAL_SLOT_COUNT
    bcc _InitAllocSizeLoop
    
    plp
    rts


; ============================================================================
; DMAPrepare - Called at the beginning of each frame to prepare for any entries
;              added on this frame.
; ---------------------------------------------------------------------------
; Expects anything
; ============================================================================
DMAPrepare:
    php
    rep #$30
    
    stz dmaTableIndex
    stz dmaTableStripIndex
    stz dmaTotalSize
    
    jsr _DMAFinalizeQueue
    
    plp
    rts


; ============================================================================
; DMATryAdd - Checks size of the desired item (stored in A). If it doesn't
;             exceed the DMA limit, the item may be added and the table index
;             is incremented. If it exceeds the limit, the overflow bit is
;             set.
; ---------------------------------------------------------------------------
; Expects any, clobbers A, X, overflow bit
; ============================================================================
DMATryAdd:
    php
    
    rep #$30
    ; Catch size zero (size 65536)
    beq _DontDMA
    
    ldx dmaTableIndex
    cpx #DMA_TABLE_MAIN_SIZE - 2
    bcs _DontDMA
    
    sta dmaTableSize, x
    adc dmaTotalSize
    cmp #DMA_TOTAL_SIZE_LIMIT
    bcs _DontDMA
    
    adc #DMA_OVERHEAD
    sta dmaTotalSize
    txa
    inc a
    inc a
    sta dmaTableIndex
    
    plp
    clv
    rts
    
_DontDMA:
    
    plp
    sep #$40 ; Overflow flag indicates we hit DMA limit
    rts


; ============================================================================
; DMATryAddStrip4 - Checks size of the desired item (stored in A) as if it
;             were the size of one strip of four strips total. If it doesn't
;             exceed the DMA limit, the strips may be added and the table
;             index is incremented. If it exceeds the limit, the overflow bit
;             is set.
; ---------------------------------------------------------------------------
; Expects any, clobbers A, X, overflow bit
; ============================================================================
DMATryAddStrip4:
    php
    
    rep #$30
    ; Catch size zero (size 65536)
    beq _DontDMA
    
    ldx dmaTableStripIndex
    cpx #DMA_TABLE_STRIP_SIZE - 2
    bcs _DontDMA
    
    sta dmaTableStripSize, x
    asl
    asl
    adc dmaTotalSize
    adc #DMA_OVERHEAD_STRIP4
    cmp #DMA_TOTAL_SIZE_LIMIT
    bcs _DontDMA
    
    sta dmaTotalSize
    txa
    inc a
    inc a
    sta dmaTableStripIndex
    
    plp
    clv
    rts


; ============================================================================
; Called on the first DMATryQueueAdd for a given frame. Reads as many items off
;  the queue as possible, and sets their priority appropriately. Adds to the
;  running count of bytes sent via DMATryQueueAdd.
; ============================================================================
_DMAQueuePrioritizeItems:
    lda dmaTotalSize
    sta dmaQueueTotalSize
    
    phx
    phy
    
    ; We haven't popped anything yet this frame
    stz dmaQueuePoppedNum
    
    ; Check for empty queue
    lda dmaQueueTailLength
    beq _ReadOffQueueDone
    
    ; Calculate tail position using head
    lda dmaQueueHead
    sec
    sbc dmaQueueTailLength
    sec
    sbc dmaQueueTailLength
    and #DMA_QUEUE_MASK
    tay
    
_ReadOffQueueLoop:
    
    ; Look at the next item in the queue
    ldx dmaQueueItems, y ; X is now the slot ID
    lda dmaQueueItemSize, x
    
    ; Determine if there's time to DMA this item
    clc
    adc dmaQueueTotalSize
    cmp dmaQueueMaxSize
    
    ; If we hit the limit, end
    bcs _ReadOffQueueDone
    
    sta dmaQueueTotalSize
    
    ; Give this item priority
    lda #QUEUE_PRIORITY_HIGH_UNSENT
    sta dmaQueuePriority, x
    
    ; Update queue lengths
    inc dmaQueuePoppedNum
    dec dmaQueueTailLength
    beq _ReadOffQueueDone
    
    ; Calculate new tail position
    tya
    inc a
    inc a
    and #DMA_QUEUE_MASK
    tay
    
    bra _ReadOffQueueLoop
    
_ReadOffQueueDone:
    
    ply
    plx
    
    rts


; ============================================================================
; DMATryQueueAdd - Checks size of the desired item (stored in A) using the
;                  slot ID in Y. If it doesn't exceed the DMA queue limit, the
;                  item may be added and the table index is incremented. If it
;                  exceeds the limit, it is queued up for a future frame, and
;                  the overflow bit is set. Each frame, this function should be
;                  called again until the request goes through.
; ---------------------------------------------------------------------------
; Expects any, clobbers A, X, Y, overflow bit
; ============================================================================
DMATryQueueAdd:
    php
    
    rep #$30
    
    ; Update the item's size ahead of time
    sta _dmaScratch
    
    ; If this is the first request, prioritize as many items as possible.
    lda dmaQueuePoppedNum
    bpl _QueueItemsPrioritized
    jsr _DMAQueuePrioritizeItems
    
_QueueItemsPrioritized:

    ; If this item has priority, just add it to the table
    lda dmaQueuePriority, y
    cmp #QUEUE_PRIORITY_HIGH_UNSENT
    bcs _DoTablePriorityAdd
    
    ; If there's time left for the queue, add to the table
    lda _dmaScratch
    adc #DMA_OVERHEAD
    sta dmaQueueItemSize, y
    adc dmaQueueTotalSize
    cmp dmaQueueMaxSize
    
    ; Otherwise, add to the queue
    bcs _DoQueueAdd

    sta _dmaNewTotalSize
    lda _dmaScratch
    jsr DMATryAdd
    bvs _DoQueueAdd
    
    lda #QUEUE_PRIORITY_NONE
    sta dmaQueuePriority, y
    
    lda _dmaNewTotalSize
    sta dmaQueueTotalSize
    
    plp
    clv
    rts

_DoTablePriorityAdd:
    lda _dmaScratch
    jsr DMATryAdd
    bvs _DoQueueUpdate
    
    lda #QUEUE_PRIORITY_NONE
    sta dmaQueuePriority, y
    
    plp
    clv
    rts

    
; ============================================================================
; Adds the object to the queue if it couldn't be placed in the table.
; ============================================================================
_DoQueueAdd:
    lda dmaQueuePriority, y
    bne _AlreadyQueued
    
    ; Mark as already queued
    lda #QUEUE_PRIORITY_QUEUED
    sta dmaQueuePriority, y
    
    tya ; A is now the slot ID
    ldy dmaQueueHead
    sta dmaQueueItems, y
    
    tya ; A is the new queue head pos
    inc a
    inc a
    and #DMA_QUEUE_MASK
    sta dmaQueueHead

    inc dmaQueueTailLength
    
_AlreadyQueued:
    plp
    sep #$40 ; Overflow flag indicates we hit DMA limit
    rts

; ============================================================================
; DMATryQueueAddStrip4 - Checks size of the desired strip item, with the size
;                        of a single strip stored in A, and the slot ID in Y.
;                        If it doesn't exceed the DMA queue limit, the item
;                        may be added and the table index is incremented. If
;                        it exceeds the limit, it is queued up for a future
;                        frame, and the overflow bit is set. Each frame, this
;                        function should be called again until the request
;                        goes through.
; ---------------------------------------------------------------------------
; Expects any, clobbers A, X, Y, overflow bit
; ============================================================================
DMATryQueueAddStrip4:
    php
    
    rep #$30
    
    ; Update the item's size ahead of time
    sta _dmaScratch
    
    ; If this is the first request, prioritize as many items as possible.
    lda dmaQueuePoppedNum
    bpl _QueueItemsPrioritizedStrip
    jsr _DMAQueuePrioritizeItems
    
_QueueItemsPrioritizedStrip:

    ; If this item has priority, just add it to the table
    lda dmaQueuePriority, y
    cmp #QUEUE_PRIORITY_HIGH_UNSENT
    bcs _DoTablePriorityAddStrip
    
    ; If there's time left for the queue, add to the table
    lda _dmaScratch
    asl
    asl
    adc #DMA_OVERHEAD_STRIP4
    sta dmaQueueItemSize, y
    adc dmaQueueTotalSize
    cmp dmaQueueMaxSize
    
    ; Otherwise, add to the queue
    bcs _DoQueueAdd

    sta _dmaNewTotalSize
    lda _dmaScratch
    jsr DMATryAddStrip4
    bvs _DoQueueAdd
    
    lda #QUEUE_PRIORITY_NONE
    sta dmaQueuePriority, y
    
    lda _dmaNewTotalSize
    sta dmaQueueTotalSize
    
    plp
    clv
    rts

_DoTablePriorityAddStrip:
    lda _dmaScratch
    jsr DMATryAddStrip4
    bvs _DoQueueUpdate
    
    lda #QUEUE_PRIORITY_NONE
    sta dmaQueuePriority, y
    
    plp
    clv
    rts


_DoQueueUpdate:
    ; Already in the queue, say this was delayed so it doesn't get culled out
    lda #QUEUE_PRIORITY_HIGH_DELAYED
    sta dmaQueuePriority, y
    
    plp
    sep #$40 ; Overflow flag indicates we hit DMA limit
    rts

; ============================================================================
; Performs internal compacting of the DMA queue, discarding any items that we
;  don't want to keep priority for
; ============================================================================
_DMAFinalizeQueue:
    php
    
    rep #$30
    
    ; At the end of the frame, keep any prioritized but un-tabled items on the queue
    ;  (assuming they're still trying to transfer), by collapsing the head of the queue
    ;  to remove any already transferred items
    
    ; X is the queue index to inspect
    ; Y is the destination queue index
    ; X is also the queue item ID
    
    ; If nothing in the queue was popped, we don't need to do anything
    lda dmaQueuePoppedNum
    beq _QueueCompactDone
    bmi _QueueCompactDone
    
    ; Compute tail pos, and use it as our starting point for both counters
    lda dmaQueueHead
    sec
    sbc dmaQueueTailLength
    sec
    sbc dmaQueueTailLength
    and #DMA_QUEUE_MASK
    tax
    tay
    
_QueueCompactLoop:
    ; Read the next queue item
    lda dmaQueueItems, x
    phx
    tax
    ; Look up its priority. We only keep ones that have been delayed:
    ; - If an entry was in the queue and transferred, remove it.
    ; - If an entry was in the queue and no attempt was made to
    ;     transfer it, remove it. This culls "zombie" entries
    ; - If an entry was in the queue and couldn't be transferred
    ;     because there wasn't enough DMA time, keep it in the queue.
    lda dmaQueuePriority, x
    cmp #QUEUE_PRIORITY_HIGH_DELAYED
    beq _QueueKeepItem
    
    stz dmaQueuePriority, x
    bra _QueueNextItem
    
_QueueKeepItem:

    txa
    sta dmaQueueItems, y
    
    inc dmaQueueTailLength
    
    tya
    dec a
    dec a
    and #DMA_QUEUE_MASK
    tay
    
_QueueNextItem:
    pla
    
    dec a
    dec a
    and #DMA_QUEUE_MASK
    tax
    
    dec dmaQueuePoppedNum
    bne _QueueCompactLoop
    
_QueueCompactDone:
    
    lda #$8000
    sta dmaQueuePoppedNum
    
    plp
    rts


; ============================================================================
; Expects anything but chops XY, clobbers A, X, Y
; ============================================================================
DMADoTransfer:
    php
    
    rep #$20
    sep #$10
    
    phd
    lda #$4300 ; For faster DMA register accesses
    tcd
    
    ;     da ifttt DMA transfer mode
    ldy #%00000001
    sty $00
    ldy #$18    ; VRAM write as destination
    sty $01
    ldy #$80
    sty $2115   ; VMAIN normal, word access
    
    ldx dmaTableIndex
    beq _FinishMainDMATransfer
    
_MainDMATransferLoop:

    ; NOTICE: If this is modified, the DMA_OVERHEAD constants must also
    ;         be updated!
    
    lda dmaTableSrc - 2, x
    sta $02
    ldy dmaTableSrcBank - 2, x
    sty $04
    lda dmaTableDest - 2, x
    sta $2116
    lda dmaTableSize - 2, x
    sta $05
    ldy #$01
    sty $420b
    
    dex
    dex
    bne _MainDMATransferLoop
    
_FinishMainDMATransfer:

    ldx dmaTableStripIndex
    beq _FinishStripDMATransfer
    
_StripDMATransferLoop:

    ; NOTICE: If this is modified, the DMA_OVERHEAD constants must also
    ;         be updated!
    
    ; Strip 1
    
    ldy dmaTableStripSrcBank - 2, x
    sty $04
    lda dmaTableStripSize - 2, x
    sta $05
    
    lda dmaTableStripSrc - 2, x
    sta $02
    
    lda dmaTableStripDest - 2, x
    sta $2116
    
    ldy #$01
    sty $420b
    
    ; Strip 2
    
    clc
    
    lda dmaTableStripSize - 2, x
    sta $05
    
    lda dmaTableStripDest - 2, x
    adc #$100
    sta $2116
    
    ldy #$01
    sty $420b
    
    ; Strip 3
    
    lda dmaTableStripSize - 2, x
    sta $05
    
    lda dmaTableStripDest - 2, x
    adc #$200
    sta $2116
    
    ldy #$01
    sty $420b
    
    ; Strip 4
    
    lda dmaTableStripSize - 2, x
    sta $05
    
    lda dmaTableStripDest - 2, x
    adc #$300
    sta $2116
    
    ldy #$01
    sty $420b
    
    dex
    dex
    bne _StripDMATransferLoop
    
_FinishStripDMATransfer:
    
    pld
    plp
    rts


; ============================================================================
; VramAllocateSlot - Finds and allocates an empty slot in VRAM for sprite
;                    streaming. If successful, returns the full slot ID found
;                    in A. Otherwise the overflow bit is set.
; ---------------------------------------------------------------------------
; In: A -- Slot search range - low byte: last main slot to search (exclusive),
;                              high byte: first main slot to search
;     Y -- Size of block
; ---------------------------------------------------------------------------
; Expects anything, clobbers A, D
; ============================================================================
VramAllocateSlot:
    php
    rep #$30
    phx
    phy
    
    sep #$30
    sta _dmaScratch
    
    rep #$20
    tyx
    
    ; Set current slot to first slot
    xba
    and #$ff
    tay; Y is now the current slot we're searching
    
    lda _AllocationTables, x
    tcd ; D is now the start of the allocation lookup table
    
    sep #$20
    
    lda #$ff
    sta _slotCandidate
    
_SlotSearchLoop:
    ; While current slot is less or equal to last slot
    cpy _dmaScratch
    bcs _SlotSearchDone
    
    ; Get current slot contents, put in index
    ldx vramSlotAllocated, y
    
    lda z:0, x
    ; If resulting allocation is not possible
    ; Then go to next slot
    bmi _NextSlot
    
    ; If resulting allocation is perfect
    ; Then exit early
    cmp #%1111
    beq _SlotSearchFoundPreferred
    
    ; Otherwise set candidate to current slot and go to next slot
    sty _slotCandidate
_NextSlot:
    iny
    bra _SlotSearchLoop

_SlotSearchDone:
    
    ; If there wasn't a candidate
    ; Then set overflow bit and exit early
    ldy _slotCandidate
    bmi _NoCandidate
    
    ; Set the new state of the slot based on AllocationNewState
    ldx vramSlotAllocated, y
    lda z:0, x

_SlotSearchFoundPreferred:
    sta vramSlotAllocated, y
    
    ; Get slot index we just allocated
    lda z:32, x
    clc
    adc _MainSlotToFullSlot, y
    
    rep #$30
    and #$ff
    tay
    
    ; Get the diff between old slot state and new
    sep #$20
    lda z:16, x
    tyx
    sta f:vramSlotAllocatedDiff, x ; Remember that diff for dealloc
    
    rep #$30
    txa
    
    ply
    plx
    plp
    clv
    rts

_NoCandidate:
    rep #$30
    ply
    plx
    plp
    sep #$40 ; Set overflow flag
    rts


; ============================================================================
; VramDeallocateSlot - Deallocates a previously allocated block of VRAM, so
;                      that the slot(s) can be reused.
; ---------------------------------------------------------------------------
; In: Y -- Full slot ID to deallocate.
; ---------------------------------------------------------------------------
; Expects anything, clobbers A
; ============================================================================
VramDeallocateSlot:
    php
    rep #$30
    phx
    
    tyx
    tya
    lsr
    lsr
    tay
    
    sep #$20
    lda f:vramSlotAllocatedDiff, x
    bmi _DontDeallocate
    
    eor vramSlotAllocated, y
    sta vramSlotAllocated, y
    lda #$ff
    sta f:vramSlotAllocatedDiff, x
    
    rep #$30
    txy
    plx
    plp
    clv
    rts
    
_DontDeallocate:
    rep #$30
    txy
    plx
    plp
    sep #$40 ; Set overflow flag
    rts


; ============================================================================
; Lookup tables for the slot allocation

_MainSlotToFullSlot:
    .byte $00
    .byte $04
    .byte $08
    .byte $0c
    .byte $10
    .byte $14
    .byte $18
    .byte $1c

_AllocationTables:
    .word .loword(_AllocationNewState1)
    .word .loword(_AllocationNewState2)
    .word .loword(_AllocationNewState3)
    .word .loword(_AllocationNewState4)

_AllocationNewState1:
    .byte %1000     ; .... -> *...
    .byte %1001     ; ...O -> *..O
    .byte %0011     ; ..O. -> ..O*
    .byte %1011     ; ..OO -> *.OO
    .byte %1100     ; .O.. -> *O..
    .byte %1101     ; .O.O -> *O.O
    .byte %1110     ; .OO. -> *OO.
    .byte %1111     ; .OOO -> *OOO
    .byte %1001     ; O... -> O..*
    .byte %1101     ; O..O -> O*.O
    .byte %1011     ; O.O. -> O.O*
    .byte %1111     ; O.OO -> O*OO
    .byte %1101     ; OO.. -> OO.*
    .byte %1111     ; OO.O -> OO*O
    .byte %1111     ; OOO. -> OOO*
    .byte $ff       ; OOOO -> none

_AllocationDiff1:
    .byte %1000     ; .... -> *...
    .byte %1000     ; ...O -> *..O
    .byte %0001     ; ..O. -> ..O*
    .byte %1000     ; ..OO -> *.OO
    .byte %1000     ; .O.. -> *O..
    .byte %1000     ; .O.O -> *O.O
    .byte %1000     ; .OO. -> *OO.
    .byte %1000     ; .OOO -> *OOO
    .byte %0001     ; O... -> O..*
    .byte %0100     ; O..O -> O*.O
    .byte %0001     ; O.O. -> O.O*
    .byte %0100     ; O.OO -> O*OO
    .byte %0001     ; OO.. -> OO.*
    .byte %0010     ; OO.O -> OO*O
    .byte %0001     ; OOO. -> OOO*
    .byte $ff       ; OOOO -> none

_AllocationSubslot1:
    .byte $00
    .byte $00
    .byte $03
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $03
    .byte $01
    .byte $03
    .byte $01
    .byte $03
    .byte $02
    .byte $03
    .byte $ff

_AllocationNewState2:
    .byte %1100     ; .... -> **..
    .byte %1101     ; ...O -> **.O
    .byte %1110     ; ..O. -> **O.
    .byte %1111     ; ..OO -> **OO
    .byte %0111     ; .O.. -> .O**
    .byte $ff       ; .O.O -> none
    .byte $ff       ; .OO. -> none
    .byte $ff       ; .OOO -> none
    .byte %1011     ; O... -> O.**
    .byte %1111     ; O..O -> O**O
    .byte $ff       ; O.O. -> none
    .byte $ff       ; O.OO -> none
    .byte %1111     ; OO.. -> OO**
    .byte $ff       ; OO.O -> none
    .byte $ff       ; OOO. -> none
    .byte $ff       ; OOOO -> none

_AllocationDiff2:
    .byte %1100     ; .... -> **..
    .byte %1100     ; ...O -> **.O
    .byte %1100     ; ..O. -> **O.
    .byte %1100     ; ..OO -> **OO
    .byte %0011     ; .O.. -> .O**
    .byte $ff       ; .O.O -> none
    .byte $ff       ; .OO. -> none
    .byte $ff       ; .OOO -> none
    .byte %0011     ; O... -> O.**
    .byte %0110     ; O..O -> O**O
    .byte $ff       ; O.O. -> none
    .byte $ff       ; O.OO -> none
    .byte %0011     ; OO.. -> OO**
    .byte $ff       ; OO.O -> none
    .byte $ff       ; OOO. -> none
    .byte $ff       ; OOOO -> none

_AllocationSubslot2:
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $02
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $02
    .byte $01
    .byte $ff
    .byte $ff
    .byte $02
    .byte $ff
    .byte $ff
    .byte $ff

_AllocationNewState3:
    .byte %1110     ; .... -> ***.
    .byte %1111     ; ...O -> ***O
    .byte $ff       ; ..O. -> none
    .byte $ff       ; ..OO -> none
    .byte $ff       ; .O.. -> none
    .byte $ff       ; .O.O -> none
    .byte $ff       ; .OO. -> none
    .byte $ff       ; .OOO -> none
    .byte %1111     ; O... -> O***
    .byte $ff       ; O..O -> none
    .byte $ff       ; O.O. -> none
    .byte $ff       ; O.OO -> none
    .byte $ff       ; OO.. -> none
    .byte $ff       ; OO.O -> none
    .byte $ff       ; OOO. -> none
    .byte $ff       ; OOOO -> none

_AllocationDiff3:
    .byte %1110     ; .... -> ***.
    .byte %1110     ; ...O -> ***O
    .byte $ff       ; ..O. -> none
    .byte $ff       ; ..OO -> none
    .byte $ff       ; .O.. -> none
    .byte $ff       ; .O.O -> none
    .byte $ff       ; .OO. -> none
    .byte $ff       ; .OOO -> none
    .byte %0111     ; O... -> O***
    .byte $ff       ; O..O -> none
    .byte $ff       ; O.O. -> none
    .byte $ff       ; O.OO -> none
    .byte $ff       ; OO.. -> none
    .byte $ff       ; OO.O -> none
    .byte $ff       ; OOO. -> none
    .byte $ff       ; OOOO -> none

_AllocationSubslot3:
    .byte $00
    .byte $00
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $01
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff

_AllocationNewState4:
    .byte %1111     ; .... -> ****
    .byte $ff       ; ...O -> none
    .byte $ff       ; ..O. -> none
    .byte $ff       ; ..OO -> none
    .byte $ff       ; .O.. -> none
    .byte $ff       ; .O.O -> none
    .byte $ff       ; .OO. -> none
    .byte $ff       ; .OOO -> none
    .byte $ff       ; O... -> none
    .byte $ff       ; O..O -> none
    .byte $ff       ; O.O. -> none
    .byte $ff       ; O.OO -> none
    .byte $ff       ; OO.. -> none
    .byte $ff       ; OO.O -> none
    .byte $ff       ; OOO. -> none
    .byte $ff       ; OOOO -> none

_AllocationDiff4:
    .byte %1111     ; .... -> ****
    .byte $ff       ; ...O -> none
    .byte $ff       ; ..O. -> none
    .byte $ff       ; ..OO -> none
    .byte $ff       ; .O.. -> none
    .byte $ff       ; .O.O -> none
    .byte $ff       ; .OO. -> none
    .byte $ff       ; .OOO -> none
    .byte $ff       ; O... -> none
    .byte $ff       ; O..O -> none
    .byte $ff       ; O.O. -> none
    .byte $ff       ; O.OO -> none
    .byte $ff       ; OO.. -> none
    .byte $ff       ; OO.O -> none
    .byte $ff       ; OOO. -> none
    .byte $ff       ; OOOO -> none

_AllocationSubslot4:
    .byte $00
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
    .byte $ff
