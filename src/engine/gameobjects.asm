; Game object handling


.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/constants.inc"
.include "../../inc/engine/crashhandler.inc"
.include "../../inc/engine/gameobjects.inc"

.import scratch, AddSimpleSprite, sceneTics, spriteScrollX, spriteScrollY
.import DMATryQueueAddStrip4, VramAllocateSlot, VramDeallocateSlot, dmaTableStripSrc, dmaTableStripDest, dmaTableStripSrcBank
.export EmptyThinker, CreateObjectPool, SetObjectPools, CreateObject, DeleteObject, DeleteObjectNoCallback, ResetAllObjects, UpdateObjects, MoveObject, DespawnObjectIfOutside, AnimateDrawSimpleSprite, AnimateWithCallback, SetAnimation, ObjectAllocateVram, ObjectDeallocateVram, StreamDynamicSprite, ObjectWorldToTile

_POOL_BEGIN = $00
_POOL_END = $02

.macro Macro_MapObjVar LONAME, MIDNAME, HINAME
    LONAME: .res 1
    .export LONAME
    MIDNAME: .res 1
    .export MIDNAME
    HINAME: .res OBJ_VAR_SIZE - 2
    .export HINAME
.endmacro

.macro Macro_MapObjVarWord NAME
    NAME: .res OBJ_VAR_SIZE
    .export NAME
.endmacro

.macro Macro_MapObjVarByteWord BYTENAME, WORDNAME
    BYTENAME: .res 1
    .export BYTENAME
    WORDNAME: .res OBJ_VAR_SIZE - 1
    .export WORDNAME
.endmacro

.segment "LORAM"

Macro_MapObjVar objPosSubX, objPosX, objPosHiX
Macro_MapObjVar objPosSubY, objPosY, objPosHiY
Macro_MapObjVarByteWord objCustomByteA, objVelX
Macro_MapObjVarByteWord objCustomByteB, objVelY
Macro_MapObjVarByteWord objTimer, objAnimDef
Macro_MapObjVarByteWord objCustomByteC, objAttribute
Macro_MapObjVarByteWord objCustomByteD, objDespawnBox
Macro_MapObjVarWord objCustomA
Macro_MapObjVarWord objCustomB
Macro_MapObjVarWord objCustomC
Macro_MapObjVarWord objCustomD

Macro_MapObjVarByteWord objFlags, objCleanupFunc
Macro_MapObjVarWord objThinker
_objThinkSentinel: .res 2 ; Must come directly after objThinker!

objectPoolHead: .res 16
objectPoolTail: .res 16
objectPoolStart: .res 16
objectPoolEnd: .res 16
objectPool: .res OBJ_MAX_COUNT * 2

_callbackScratch: .res 2

.segment "HIRAM"

Macro_MapObjVarWord objPoolId
Macro_MapObjVarByteWord objVramSlot, objVramDest
Macro_MapObjVarWord objDynSpritedef
Macro_MapObjVarWord objCustomHiA
Macro_MapObjVarWord objCustomHiB
Macro_MapObjVarWord objCustomHiC
Macro_MapObjVarWord objCustomHiD
Macro_MapObjVarWord objCustomHiE
Macro_MapObjVarWord objCustomHiF
Macro_MapObjVarWord objCustomHiG
Macro_MapObjVarWord objCustomHiH

.segment "CODE0"

_DefaultObjectPool:
    .word $0000, OBJ_MAX_COUNT, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff, $ffff

Define_DynSpriteDef _EmptyDynSprite, _EmptyDynSprite, $0000, $0000


; ============================================================================
; Default thinker executed for unused slots to move to the next slot
; ============================================================================
EmptyThinker:
    inx
    inx
    inx
    jmp (objThinker, x)


; ============================================================================
; AnimateDrawSimpleSprite - Does animation processing and then draws the
;                           simple sprite from the spritedef pointed to by the
;                           animdef's data field.
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, X, Y, D (from the sprite draw)
; ============================================================================
AnimateDrawSimpleSprite:
    sep #$20
    dec objTimer, x
    rep #$20
    beq _NextAnimation
    ldy objAnimDef, x
    
_DoDraw:
    lda a:ANIMDEF_DATA, y
    tay
    jmp AddSimpleSprite

_NextAnimation:
    ldy objAnimDef, x
    lda a:ANIMDEF_NEXT, y
    sta objAnimDef, x
    tay
    sep #$20
    lda a:ANIMDEF_DELAY, y
    sta objTimer, x
    rep #$20
    
    bra _DoDraw


; ============================================================================
; AnimateWithCallback - Does animation processing and then calls the function
;                       pointed to in the spritedef's data field
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, X, Y
; ============================================================================
AnimateWithCallback:
    sep #$20
    dec objTimer, x
    rep #$20
    beq _NextAnimationCallback
    rts

_NextAnimationCallback:
    ldy objAnimDef, x
    lda a:ANIMDEF_NEXT, y
    sta objAnimDef, x
    tay
    sep #$20
    lda a:ANIMDEF_DELAY, y
    sta objTimer, x
    rep #$20
    lda a:ANIMDEF_DATA, y
    bne _DoCallback
    
    rts

_DoCallback:
    sta _callbackScratch
    jmp (_callbackScratch)


; ============================================================================
; SetAnimation - Makes the object play a new animation.
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, X, Y
; ============================================================================
SetAnimation:
    tya
    sta objAnimDef, x
    lda a:ANIMDEF_DELAY, y
    sep #$20
    sta objTimer, x
    rep #$20
    
    rts


; ============================================================================
; CreateObjectPool - Initializes the default object pool. Called by the
;                    gameloop when switching to a new scene.
; ---------------------------------------------------------------------------
; Expects anything but chops XY, clobbers A, X
; ============================================================================
CreateObjectPool:
    php
    rep #$30
    
    lda #$0000
    ldx #$0000
_CreatePoolLoop:
    sta objectPool, x
    clc
    adc #$03
    inx
    inx
    cpx #OBJ_MAX_COUNT * 2
    bne _CreatePoolLoop
    
    ; Default object pool size
    
    ldx #.loword(_DefaultObjectPool)
    jsr SetObjectPools
    
    plp
    rts


; ============================================================================
; SetObjectPools - Initializes the object pools to user-defined values.
; ---------------------------------------------------------------------------
; In: X - pointer to list of start/end values for each pool
; ---------------------------------------------------------------------------
; Expects anything, clobbers A, X, Y
; ============================================================================
SetObjectPools:
    php
    rep #$30
    
    ldy #$0000
    
_SetObjectPoolLoop:
    lda a:_POOL_BEGIN, x
    asl
    sta objectPoolStart, y
    
    lda a:_POOL_END, x
    asl
    sta objectPoolEnd, y
    dec a
    dec a
    sta objectPoolHead, y
    sta objectPoolTail, y
    
    inx
    inx
    inx
    inx
    iny
    iny
    
    cpy #16
    bcc _SetObjectPoolLoop
    
    plp
    rts


; ============================================================================
; CreateObject - Attempts to locate an unused slot in the given object pool.
;                If a slot is found, X is set to the new object's index.
;                Otherwise the overflow bit is set.
; ---------------------------------------------------------------------------
; In: Y - index of the pool to use (word sized)
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, X
; ============================================================================
CreateObject:
    lda objectPoolHead, y
    bmi _NoObjectsLeft
    clv ; We're placing the object, caller doesn't need to clv
    ; Next slot, wrap if necessary
    inc a
    inc a
    cmp objectPoolEnd, y
    bpl _WrapObjectPool
_WrapObjectPoolDone:
    cmp objectPoolTail, y
    beq _FilledObjectPool
    
    sta objectPoolHead, y
    
_FilledObjectPoolDone:
    tax
    lda objectPool, x
    tax
    ; Remember which pool this object was from.
    tya
    sta f:objPoolId, x
    
    rts
    
_FilledObjectPool:
    ; We mark the object pool as "full" by setting the sign bit.
    ; Otherwise there is no way to tell a difference between an
    ; empty and full pool, since both will have same head and
    ; tail indices.
    ora #%1000000000000000
    sta objectPoolTail, y
    sta objectPoolHead, y
    and #%0111111111111111
    bra _FilledObjectPoolDone

_NoObjectsLeft:
    sep #$40 ; Overflow flag indicates object was not created
    rts

_WrapObjectPool:
    lda objectPoolStart, y
    bra _WrapObjectPoolDone


; ============================================================================
; DeleteObject - Deletes the object at the given index, returning the slot to
;                the pool. If the OBJFLAG_RUN_CLEANUP_FUNCTION flag is set,
;                the object's cleanup function is run before any other
;                deletion processing.
; ---------------------------------------------------------------------------
; In: X - object being released back into the pool
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, X, Y
; ============================================================================
DeleteObject:
    lda objFlags, x
    bit #OBJFLAG_RUN_CLEANUP_FUNCTION
    beq DeleteObjectNoCallback
    jsr (objCleanupFunc, x)

; ============================================================================
; DeleteObjectNoCallback - Deletes the object at the given index, returning
;                          the slot to the pool.
; ---------------------------------------------------------------------------
; In: X - object being released back into the pool
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, X, Y
; ============================================================================
DeleteObjectNoCallback:
    lda f:objPoolId, x
    tay
    lda #.loword(EmptyThinker)
    sta objThinker, x
    stz objFlags, x
    
    lda objectPoolTail, y
    bmi _UnfilledObjectPool
_UnfilledObjectPoolDone:
    inc a
    inc a
    cmp objectPoolEnd, y
    bpl _WrapObjectPoolTail
_WrapObjectPoolTailDone:
    sta objectPoolTail, y
    
    tay
    txa
    sta objectPool, y
    
    rts

_UnfilledObjectPool:
    ; See above, we clear the sign bit to indicate pool
    ; is no longer full.
    and #%0111111111111111
    sta objectPoolHead, y
    bra _UnfilledObjectPoolDone

_WrapObjectPoolTail:
    lda objectPoolStart, y
    bra _WrapObjectPoolTailDone


; ============================================================================
; ResetAllObjects - Called during gameloop scene initialization to clear all
;                   object slots by giving them the empty thinker.
; ---------------------------------------------------------------------------
; Expects anything, clobbers A, X
; ============================================================================
ResetAllObjects:
    php
    rep #$30
    
    ldx #OBJ_VAR_SIZE - 3
_ResetAllObjectsLoop:
    lda #.loword(EmptyThinker)
    sta objThinker, x
    dex
    dex
    dex
    bpl _ResetAllObjectsLoop
    plp
    rts    


; ============================================================================
; UpdateObjects - Should be called in the scene's thinker to run all object
;                 logic.
; ---------------------------------------------------------------------------
; Expects anything, clobbers X
; ============================================================================
UpdateObjects:
    php
    rep #$30
    
    ; Function pointer after the last object jumps to termination of loop
    lda #.loword(_UpdateObjectsDone)
    sta _objThinkSentinel
    
    ldx #$00
_UpdateObjectsLoop:
    jsr (objThinker, x)
    inx
    inx
    inx
    bra _UpdateObjectsLoop

_UpdateObjectsDone:
    pla ; We jsr and then jmp here, so need to pop a word from the stack
    plp
    rts


; ============================================================================
; MoveObject - Moves the object according to its X and Y velocity values.
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A
; ============================================================================
MoveObject:
    lda objVelX, x
    clc
    ; We need separate logic for moving in the positive and
    ; negative directions. This is so we know whether to inc
    ; or dec the high bit based on the carry bit status.
    bmi _MoveNegX
    
    ; Positive X
    adc objPosSubX, x
    sta objPosSubX, x
    bcc _MoveObjectY
    
    ; Inc high X
    sep #$20
    inc objPosHiX, x
    rep #$20
    
    bra _MoveObjectY
    
_MoveNegX:
    
    adc objPosSubX, x
    sta objPosSubX, x
    bcs _MoveObjectY

    ; Dec high X
    sep #$20
    dec objPosHiX, x
    rep #$20

_MoveObjectY:
    
    lda objVelY, x
    clc
    bmi _MoveNegY
    
    ; Positive Y
    adc objPosSubY, x
    sta objPosSubY, x
    bcs _IncHighY
    
    rts
    
_MoveNegY:
    
    adc objPosSubY, x
    sta objPosSubY, x
    bcc _DecHighY
    
    rts

_IncHighY:
    
    sep #$20
    inc objPosHiY, x
    rep #$20
    
    rts

_DecHighY:

    sep #$20
    dec objPosHiY, x
    rep #$20
    
    rts


; ============================================================================
; DespawnObjectIfOutside - Deletes this object if bounding box is outside
;                          the screen. For best performance, only call this
;                          if the sprite adding routine sets the overflow (V)
;                          bit.
; ---------------------------------------------------------------------------
; In: X -- Object index
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, Y
; ============================================================================
DespawnObjectIfOutside:
    ldy objDespawnBox
    
    ; This optimization switches between doing the horizontal and vertical
    ;  off-screen checks each frame. It should be virtually unnoticeable by
    ;  anyone except TASers
    lda sceneTics
    bit #$0001
    bne _VerticalDespawnCheck
    
    ; Check X is out of range
    lda objPosX, x
    
    ; X scroll and offset
    clc
    adc a:BOUNDINGBOXDEF_LEFT, y
    sec
    sbc spriteScrollX
    bmi _DespawnBoxOffLeft ; If negative, assume less than 256
    
    cmp #$0100 ; If greater than 256, it's offscreen
    bcs _DoDespawn
    
_DespawnBoxOffLeft:
    adc a:BOUNDINGBOXDEF_WIDTH, y
    bmi _DoDespawn ; If less than 0, it's offscreen
    
    rts
    
_VerticalDespawnCheck:
    
    ; Check Y is out of range
    lda objPosY, x

    ; Y scroll and offset
    clc
    adc a:BOUNDINGBOXDEF_TOP, y
    sec
    sbc spriteScrollY
    bmi _DespawnBoxOffTop ; If negative, assume less than 224
    
    cmp #$00e0 ; If greater than 224, it's offscreen
    bcs _DoDespawn
    
_DespawnBoxOffTop:
    adc a:BOUNDINGBOXDEF_HEIGHT, y
    bmi _DoDespawn ; If less than 0, it's offscreen
    
    rts

_DoDespawn:
    jsr DeleteObject


; ============================================================================
; ObjectAllocateVram - Finds and allocates an empty slot in VRAM for this
;                      object to perform sprite streaming. If successful, the
;                      overflow bit is cleared. Otherwise the bit is set.
; ---------------------------------------------------------------------------
; In: A -- Slot search range - low byte: last main slot to search (exclusive),
;                              high byte: first main slot to search
;     X -- Object index
;     Y -- Size of block
; ---------------------------------------------------------------------------
; Expects anything, clobbers A, Y, D
; ============================================================================
ObjectAllocateVram:
    php
    rep #$30
    pha
    
    ; Check a slot isn't already allocated by this object...
    sep #$20
    lda f:objVramSlot, x
    rep #$20
    bne _DontAllocatePullA
    
    pla
    
    ; Try to allocate the slot of the given size
    jsr VramAllocateSlot
    
    ; If not allocated, bail out
    bvs _DontAllocate
    
    sep #$20
    
    ; Object needs to know the OAM attribute mask, DMA destination, and VRAM slot ID
    asl
    inc a
    sta f:objVramSlot, x
    dec a
    
    rep #$20
    
    and #$ff
    tay
    
    lda _SlotToOamAttribute, y
    sta objAttribute, x
    
    lda _SlotToVramAddress, y
    sta f:objVramDest, x
    
    lda #.loword(_EmptyDynSprite)
    sta f:objDynSpritedef, x
    
    lda objFlags, x
    bit #OBJFLAG_RUN_CLEANUP_FUNCTION
    bne _DontClobberCleanupFunc
    
    ora #OBJFLAG_RUN_CLEANUP_FUNCTION
    sta objFlags, x
    
    lda #.loword(ObjectDeallocateVram)
    sta objCleanupFunc, x
    
_DontClobberCleanupFunc:
    
    plp
    clv
    rts

_DontAllocatePullA:
    pla
_DontAllocate:
    plp
    sep #$40 ; Set overflow flag
    rts


; ============================================================================
; ObjectDeallocateVram - Deallocates this object's allocated block of VRAM, so
;                        that the slot(s) can be reused.
; ---------------------------------------------------------------------------
; In: X -- Object index
; ---------------------------------------------------------------------------
; Expects anything, clobbers A, Y
; ============================================================================
ObjectDeallocateVram:
    php
    sep #$20
    
    ; Check that this object has a slot allocated.
    lda f:objVramSlot, x
    beq _DontDeallocate
    
    rep #$20
    and #$ff
    
    ; Deallocate the slot and mark the object as having no slot
    dec a
    lsr
    tay
    jsr VramDeallocateSlot
    
    lda #$00
    sta f:objVramSlot, x

_DontDeallocate:
    plp
    rts


.a16
.i16


; ============================================================================
; StreamDynamicSprite - Schedules the object's current sprite for DMA
;                       streaming if possible or necessary, and sets Y to the
;                       correct spritedef to be drawn.
; ---------------------------------------------------------------------------
; In: X -- Object index
;     Y -- DynSpritedef
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A, Y
; ============================================================================
StreamDynamicSprite:
    ; Check if this is a new sprite we need to send over
    tya
    cmp f:objDynSpritedef, x
    beq _StreamNotNeeded
    
    tcd ; Remember dynspritedef in D
    
    ; Try to do a queued DMA add
    lda f:objVramSlot, x
    dec a
    and #$ff
    
    phx
    
    tyx ; X is now dynspritedef
    tay ; Y is now the VRAM slot
    lda a:DYNSPRITEDEF_CHARLENGTH, x
    
    jsr DMATryQueueAddStrip4 ; X is now the DMA table index
    ; No DMA time, bail out.
    bvs _NoTimeToStream
    
    tdc
    tay ; Y is now dynspritedef pointer again
    
    ; Set other DMA variables.
    lda a:DYNSPRITEDEF_CHARDATABANK, y
    sta dmaTableStripSrcBank, x

    lda a:DYNSPRITEDEF_CHARDATA, y
    sta dmaTableStripSrc, x

    txy
    plx ; X is now object index
    lda f:objVramDest, x
    sta dmaTableStripDest, y
    
    ; Update the object's spritedef to display
    tdc
    sta f:objDynSpritedef, x
    
    bra _StreamDone
    
_NoTimeToStream:
    plx
    
_StreamNotNeeded:
    ; And draw using the last successfully transferred spritedef
    lda f:objDynSpritedef, x
_StreamDone:
    tay
    lda a:DYNSPRITEDEF_SPRITEDEF, y
    tay
    
    rts


; ============================================================================
; ObjectWorldToTile - Converts the object's position to a 16x16 tile index
;                     within a 128x128 region.
; ---------------------------------------------------------------------------
; Expects AXY16, clobbers A
; ============================================================================
ObjectWorldToTile:
    lda objPosSubX, x
    lsr
    lsr
    lsr
    lsr
    and #%11111
    sta scratch+2
    
    lda objPosSubY, x
    asl
    and #%0000001111100000
    ora scratch+2
    
    rts


; ============================================================================


_SlotToOamAttribute:
    .byte %00000000, %00000000
    .byte %00000100, %00000000
    .byte %00001000, %00000000
    .byte %00001100, %00000000
    .byte %01000000, %00000000
    .byte %01000100, %00000000
    .byte %01001000, %00000000
    .byte %01001100, %00000000
    .byte %10000000, %00000000
    .byte %10000100, %00000000
    .byte %10001000, %00000000
    .byte %10001100, %00000000
    .byte %11000000, %00000000
    .byte %11000100, %00000000
    .byte %11001000, %00000000
    .byte %11001100, %00000000
    .byte %00000000, %00000001
    .byte %00000100, %00000001
    .byte %00001000, %00000001
    .byte %00001100, %00000001
    .byte %01000000, %00000001
    .byte %01000100, %00000001
    .byte %01001000, %00000001
    .byte %01001100, %00000001
    .byte %10000000, %00000001
    .byte %10000100, %00000001
    .byte %10001000, %00000001
    .byte %10001100, %00000001
    .byte %11000000, %00000001
    .byte %11000100, %00000001
    .byte %11001000, %00000001
    .byte %11001100, %00000001

_SlotToVramAddress:
    .word $0000
    .word $0040
    .word $0080
    .word $00c0
    .word $0400
    .word $0440
    .word $0480
    .word $04c0
    .word $0800
    .word $0840
    .word $0880
    .word $08c0
    .word $0c00
    .word $0c40
    .word $0c80
    .word $0cc0
    .word $1000
    .word $1040
    .word $1080
    .word $10c0
    .word $1400
    .word $1440
    .word $1480
    .word $14c0
    .word $1800
    .word $1840
    .word $1880
    .word $18c0
    .word $1c00
    .word $1c40
    .word $1c80
    .word $1cc0
    
