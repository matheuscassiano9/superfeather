
; InitSNES originally written by Neviksti. It has been ported to ca65.

.p816   ; 65816 processor
.smart

.export InitializeSNES

.segment "CODE1"

InitializeSNES:
  phk			;set Data Bank = Program Bank
  plb

  lda #$0000	;set Direct Page = $0000
  tcd			;Transfer Accumulator to Direct Register

  ldx $1FFD		;we clear all the mem at one point ...
  stx $4372  	;so save the return address in a place that won't get overwritten
  ldx $1FFF
  stx $4374

  sep #$20		; mem/A = 8 bit
  rep #$10

  lda #$8F
  sta $2100		;turn screen off for now, set brightness to normal

  ldx #$2101
_Loop00:		;regs $2101-$210C
  stz $00,X		;set Sprite,Character,Tile sizes to lowest, and set addresses to $0000
  inx
  cpx #$210D
  bne _Loop00

_Loop01:		;regs $210D-$2114
  stz $00,X		;Set all BG scroll values to $0000
  stz $00,X
  inx
  cpx #$2115
  bne _Loop01

  lda #$80		;reg $2115
  sta $2115		; Initialize VRAM transfer mode to word-access, increment by 1

  stz $2116		;regs $2117-$2117
  stz $2117		;VRAM address = $0000

			;reg $2118-$2119
			;VRAM write register... don't need to initialize

  stz $211A		;clear Mode7 setting

  ldx #$211B
_Loop02:		;regs $211B-$2120
  stz $00,X		;clear out the Mode7 matrix values
  stz $00,X
  inx
  cpx #$2121
  bne _Loop02

			;reg $2121 - Color address, doesn't need initilaizing
			;reg $2122 - Color data, is initialized later

  ldx #$2123
_Loop03:		;regs $2123-$2133
  stz $00,X		;turn off windows, main screens, sub screens, color addition,
  inx			;fixed color = $00, no super-impose (external synchronization),
  cpx #$2132	;no interlaced mode, normal resolution
  bne _Loop03
  
  lda #%11100000 ; Danni fix: Set fixed color value correctly.
  sta $2132
  stz $2133

			;regs $2134-$2136  - multiplication result, no initialization needed
			;reg $2137 - software H/V latch, no initialization needed
			;reg $2138 - Sprite data read, no initialization needed
			;regs $2139-$213A  - VRAM data read, no initialization needed
			;reg $213B - Color RAM data read, no initialization needed
			;regs $213C-$213D  - H/V latched data read, no initialization needed

  stz $213E		;reg $213E - might not be necesary, but selects PPU master/slave mode
			;reg $213F - PPU status flag, no initialization needed

			;reg $2140-$2143 - APU communication regs, no initialization required

			;reg $2180  -  read/write WRAM register, no initialization required
			;reg $2181-$2183  -  WRAM address, no initialization required

			;reg $4016-$4017  - serial JoyPad read registers, no need to initialize


  stz $4200		;reg $4200  - disable timers, NMI,and auto-joyread

  lda #$FF
  sta $4201		;reg $4201  - programmable I/O write port, initalize to allow reading at in-port

			;regs $4202-$4203  - multiplication registers, no initialization required
			;regs $4204-$4206  - division registers, no initialization required

			;regs $4207-$4208  - Horizontal-IRQ timer setting, since we disabled this, it is OK to not init
			;regs $4209-$420A  - Vertical-IRQ timer setting, since we disabled this, it is OK to not init

  stz $420B		;reg $420B  - turn off all general DMA channels
  stz $420C		;reg $420C  - turn off all H-MA channels

  stz $420D		;reg $420D  - ROM access time to slow (2.68Mhz)

  lda $4210		;reg $4210  - NMI status, reading resets

			;reg $4211  - IRQ status, no need to initialize
			;reg $4212  - H/V blank and JoyRead status, no need to initialize
			;reg $4213  - programmable I/O inport, no need to initialize

			;reg $4214-$4215  - divide results, no need to initialize
			;reg $4216-$4217  - multiplication or remainder results, no need to initialize

			;regs $4218-$421f  - JoyPad read registers, no need to initialize

			;regs $4300-$437F
			;no need to intialize because DMA was disabled above
			;also, we're not sure what all of the registers do, so it is better to leave them at
			;their reset state value

  jsr ClearVRAM      ;Reset VRAM
  jsr ClearPalette   ;Reset colors

  ;**** clear Sprite tables ********

  stz $2102	;sprites initialized to be off the screen, palette 0, character 0
  stz $2103
  ldx #$0080
  lda #$F0
_Loop08:
  sta $2104	;set X = 240
  sta $2104	;set Y = 240
  stz $2104	;set character = $00
  stz $2104	;set priority=0, no flips
  dex
  bne _Loop08

  ldx #$0020
_Loop09:
  stz $2104		;set size bit=0, x MSB = 0
  dex
  bne _Loop09

  ;**** clear WRAM ********

  stz $2181		;set WRAM address to $000000
  stz $2182
  stz $2183

  ldx #$8008
  stx $4300         ;Set DMA mode to fixed source, BYTE to $2180
  ldx #.loword(wram_fill_byte)
  stx $4302         ;Set source offset
  lda #<.bank(wram_fill_byte)
  sta $4304         ;Set source bank
  ldx #$0000
  stx $4305         ;Set transfer size to 64k bytes
  lda #$01
  sta $420B         ;Initiate transfer

  lda #$01          ;now set the next 64k bytes
  sta $420B         ;Initiate transfer

  phk			;make sure Data Bank = Program Bank
  plb

  cli			;enable interrupts again

  ldx $4372  	;get our return address...
  stx $1FFD
  lda $4374
  sta $1FFF
  rtl

wram_fill_byte:
.byte $00

;----------------------------------------------------------------------------
; ClearVRAM -- Sets every byte of VRAM to zero
; In: None
; Out: None
; Modifies: flags
;----------------------------------------------------------------------------
ClearVRAM:
   pha
   phx
   php

   rep #$30		; mem/A = 8 bit, X/Y = 16 bit
   sep #$20

   lda #$80
   sta $2115         ;Set VRAM port to word access
   ldx #$1809
   stx $4300         ;Set DMA mode to fixed source, WORD to $2118/9
   ldx #$0000
   stx $2116         ;Set VRAM port address to $0000
   stx $0000         ;Set $00:0000 to $0000 (assumes scratchpad ram)
   stx $4302         ;Set source address to $xx:0000
   lda #$00
   sta $4304         ;Set source bank to $00
   ldx #$FFFF
   stx $4305         ;Set transfer size to 64k-1 bytes
   lda #$01
   sta $420B         ;Initiate transfer

   stz $2119         ;clear the last byte of the VRAM

   plp
   plx
   pla
   rts

;----------------------------------------------------------------------------
; ClearPalette -- Reset all palette colors to zero
; In: None
; Out: None
; Modifies: flags
;----------------------------------------------------------------------------
ClearPalette:
   phx
   php
   rep #$30		; mem/A = 8 bit, X/Y = 16 bit
   sep #$20

   stz $2121
   ldx #$0100
ClearPaletteLoop:
   stz $2122
   stz $2122
   dex
   bne ClearPaletteLoop

   plp
   plx
   rts
