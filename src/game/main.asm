
.p816   ; 65816 processor
.a16
.i16
.smart

.import Gameloop, sceneInitFunc, sceneThinkFunc

.export GameMain, SpcDriver

.segment "DATA3"

SpcDriver:
    .incbin "data/audio/spc700.bin"

.segment "CODE0"


; ============================================================================
; GameMain is called once the engine has finished its own initialization. This space is for your game
;  to perform its own setup.
; ============================================================================
GameMain:
    
    ; Set your initial scenes here by writing to sceneInitFunc and sceneThinkFunc

_Forever:
    jsr Gameloop
    bra _Forever
