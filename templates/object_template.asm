
.p816   ; 65816 processor
.a16
.i16
.smart

.include "../../inc/engine/gameobjects.inc"

.import objThinker
.export MyObjectThinker

.segment "CODE0"


; ============================================================================
; The thinker function associated with this object. Called once a frame for
;  all objects of this type.
; ============================================================================
MyObjectThinker:
    ; Code goes here
    Macro_NextThinker
