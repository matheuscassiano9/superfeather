
local function fatalError(message)
    print(message)
    os.exit()
end

local function getDirectoryPath(filename)
    local lastChar = #filename
    
    for i = #filename, 1, -1 do
        if filename:byte(i) == string.byte("/") then
            lastChar = i
            break
        end
    end
    
    return filename:sub(0, lastChar)
end

local function tryExecute(command, platform)
    if platform == "windows" then
        local winCommand = command:gsub("/", "\\")
        return os.execute(winCommand)
    else
        return os.execute(command)
    end
end

local function makeDirectory(dirname, platform)
    local success = false
    
    if platform == "linux" then
        success = tryExecute("mkdir -p " .. dirname, platform)
    elseif platform == "windows" then
        success = tryExecute("if not exist \"" .. dirname .. "\" mkdir " .. dirname, platform)
    else
        fatalError("Unknown platform.")
    end
    
    return success
end

local function assembleFile(source, outList, bintoolsPath, platform)
    local outFilename = ".build/" .. source .. ".o"
    
    table.insert(outList, outFilename)
    
    local outDir = getDirectoryPath(outFilename)
    if not makeDirectory(outDir, platform) then
        fatalError("Could not make build directories.")
    end
    
    print("- Assembling " .. source .. " -> " .. outFilename)
    local result = tryExecute(bintoolsPath .. "/ca65 " .. source .. " -o " .. outFilename, platform)
    
    return result
end

local function linkFiles(objectList, target, linkerConfig, bintoolsPath, platform)
    local args = "-C " .. linkerConfig .. " -m " .. target .. ".memmap -Ln " .. target .. ".sym -o " .. target .. ".sfc"
    local filesString = ""
    
    for i, v in ipairs(objectList) do
        filesString = filesString .. " " .. tostring(v)
    end
    
    args = args .. filesString
    
    print("- Linking " .. tostring(#objectList) .. " files -> " .. target)
    local result = tryExecute(bintoolsPath .. "/ld65 " .. args, platform)
    
    return result
end

local function main()
    local outList = {}
    
    local configPath = tostring(arg[1])
    local config = dofile(configPath)
    
    local platform = tostring(arg[2])
    local bintoolsPath = tostring(arg[3])
    
    for i, v in ipairs(config.fileList) do
        if not assembleFile(v, outList, bintoolsPath, platform) then
            fatalError("Could not assemble file " .. tostring(v))
        end
    end
    
    if not linkFiles(outList, config.target, config.linkerConfig, bintoolsPath, platform) then
        fatalError("Linking failed.")
    end
end

main()
