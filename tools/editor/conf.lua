function love.conf(t)
    t.title = "Heartcore Editor"
    t.author = "Danni"
    t.identity = nil
    t.version = "0.10.1"
    t.console = false
    t.release = false
    t.window.width = 800
    t.window.height = 600
    t.window.resizable = true
    t.window.fullscreen = false
    t.window.vsync = false
    t.window.fsaa = 0
    t.modules.joystick = true
    t.modules.audio = true
    t.modules.keyboard = true
    t.modules.event = true
    t.modules.image = true
    t.modules.graphics = true
    t.modules.timer = true
    t.modules.mouse = true
    t.modules.sound = true
    t.modules.physics = false
end
