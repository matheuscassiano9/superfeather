
local cabin = require "src.utils.cabin"
local oops = require "src.utils.oops"
local test = require "src.utils.test"

local DataObject = oops.BaseObject:makeClass
{
    
}

function DataObject.exists(path)
    local file = io.open(path, "rb")
    
    if file then
        file:close()
        return true
    end
    
    return false
end

function DataObject:init(path)
    if path then
        self:loadFromFile(path)
    end
end

function DataObject:loadFromFile(path)
    self.filepath = path
    
    cabin.info("Reading file: " .. self.filepath)
    
    local file, message = io.open(self.filepath, "rb")
    
    if not file then
        error("Failed to open file: " .. message)
        return
    end
    
    local fileData = file:read("*a")
    
    file:close()
    
    cabin.info("Read " .. fileData:len() .. " bytes.")
    
    self:loadData(fileData)
end

function DataObject:saveToFile()
    cabin.info("Writing file: " .. self.filepath)
    
    local file, message = io.open(self.filepath, "wb")
    
    if not file then
        error("Failed to open file: " .. message)
        return
    end
    
    local fileData = self:saveData()
    local success, message = file:write(fileData)
    
    if not success then
        error("Failed to write to file: " .. message)
    end
    
    file:close()
end

function DataObject:loadData(fileData)
    test.unusedArgs(self, fileData)
    test.implementMe()
end

function DataObject:saveData()
    test.unusedArgs(self)
    test.implementMe()
end

return DataObject
