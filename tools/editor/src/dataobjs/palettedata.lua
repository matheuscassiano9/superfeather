local bit = require "bit"

local cabin = require "src.utils.cabin"
local binutil = require "src.utils.binutil"

local DataObject = require "src.dataobjs.dataobj"

local PaletteData = DataObject:makeClass
{
    
}

function PaletteData.wordToRgb(colorWord)
    local newColor = {}
    
    newColor.red = bit.band(colorWord, 31)
    newColor.green = bit.rshift(colorWord, 5)
    newColor.green = bit.band(newColor.green, 31)
    newColor.blue = bit.rshift(colorWord, 10)
    newColor.blue = bit.band(newColor.blue, 31)
    
    return newColor
end

function PaletteData.rgbToWord(color)
    local colorWord = 0
    
    colorWord = bit.bor(colorWord, color.blue)
    colorWord = bit.lshift(colorWord, 5)
    colorWord = bit.bor(colorWord, color.green)
    colorWord = bit.lshift(colorWord, 5)
    colorWord = bit.bor(colorWord, color.red)
    
    return colorWord
end

function PaletteData:loadData(fileData)
    self.colors = {}
    
    for i = 1, fileData:len() - 1, 2 do
        local colorWord = binutil.readWord(fileData, i)
        
        local newColor = PaletteData.wordToRgb(colorWord)
        
        table.insert(self.colors, newColor)
    end
    
    cabin.info("Palette has " .. #self.colors .. " colors.")
end

function PaletteData:saveData()
    local fileData = ""
    
    for _, v in ipairs(self.colors) do
        local colorWord = PaletteData.rgbToWord(v)
        
        fileData = fileData .. binutil.writeWord(colorWord)
    end
    
    return fileData
end

function PaletteData:loadStandard16()
    self.colors = {}
    
    for i = 0, 15 do
        local newColor = {}
        
        newColor.red = i * 2
        newColor.green = newColor.red
        newColor.blue = newColor.red
        
        table.insert(self.colors, newColor)
    end
end

function PaletteData:padTo256()
    local colorNum = #self.colors
    
    for _ = colorNum, 256 do
        local newColor = { red = 0, green = 0, blue = 0 }
        
        table.insert(self.colors, newColor)
    end
end

function PaletteData:getHsl(index)
    if not self.colors[index] then
        error("Bad color index")
    end
    
    local color = self.colors[index]
    
    local red = color.red / 31
    local green = color.green / 31
    local blue = color.blue / 31
    
    local minColor = math.min(red, green, blue)
    local maxColor = math.max(red, green, blue)
    local diff = maxColor - minColor
    
    local hue, sat, light
    
    if diff == 0 then
        hue = 0
    elseif maxColor == red then
        hue = (60 * (green - blue) / diff) % 360
    elseif maxColor == green then
        hue = (60 * (blue - red) / diff) + 120
    elseif maxColor == blue then
        hue = (60 * (red - green) / diff) + 240
    else
        hue = 0
    end
    
    light = (minColor + maxColor) / 2
    
    if diff == 0 then
        sat = 0
    else
        sat = diff / (1 - math.abs(2 * light - 1))
    end
    
    return hue, sat * 100, light * 100
end

return PaletteData
