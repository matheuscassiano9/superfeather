local luatexts = require "3rdparty.luatexts.lua"

local DataObject = require "src.dataobjs.dataobj"

local SerializedTable = DataObject:makeClass
{
    
}

function SerializedTable:loadData(fileData)
    local ok, data, message = luatexts.load(fileData)
    
    if not ok then
        error("Deserialization failed: " .. tostring(message))
    end
    
    self.data = data
end

function SerializedTable:saveData()
    
    return luatexts.save(self.data)
end

return SerializedTable
