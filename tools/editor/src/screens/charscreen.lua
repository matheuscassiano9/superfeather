local DropTargetScreen = require "src.screens.droptargetscreen"
local CharData = require "src.dataobjs.chardata"
local PaletteData = require "src.dataobjs.palettedata"

local CharScreen = DropTargetScreen:makeClass
{
    handlers =
    {
        filedropped = DropTargetScreen.handlers.filedropped
    },
    columns = 32,
    paletteOffset = 0
}

CharScreen.filetypeMapping =
{
    {
        extension = ".pic",
        description = "SNES char data",
        handler = function(self, file)
            self:newChar(file)
        end
    },
    {
        extension = ".pic2",
        description = "SNES char data (2bpp)",
        handler = function(self, file)
            self:newChar(file)
        end
    },
    {
        extension = ".pal",
        description = "SNES palette",
        handler = function(self, file)
            self:newPalette(file)
        end
    }
}

function CharScreen.handlers.keypressed(self, key)
    local extraPaletteOffset = 0
    
    if love.keyboard.isDown("lctrl", "rctrl") then
        extraPaletteOffset = 128
    end
    
    if key == "-" then
        self.columns = 16
        self:newTexture()
    elseif key == "=" then
        self.columns = 32
        self:newTexture()
    elseif key == "0" or key == "`" then
        self.paletteOffset = 0 + extraPaletteOffset
        self:newTexture()
    elseif key == "1" then
        self.paletteOffset = 16 + extraPaletteOffset
        self:newTexture()
    elseif key == "2" then
        self.paletteOffset = 32 + extraPaletteOffset
        self:newTexture()
    elseif key == "3" then
        self.paletteOffset = 48 + extraPaletteOffset
        self:newTexture()
    elseif key == "4" then
        self.paletteOffset = 64 + extraPaletteOffset
        self:newTexture()
    elseif key == "5" then
        self.paletteOffset = 80 + extraPaletteOffset
        self:newTexture()
    elseif key == "6" then
        self.paletteOffset = 96 + extraPaletteOffset
        self:newTexture()
    elseif key == "7" then
        self.paletteOffset = 112 + extraPaletteOffset
        self:newTexture()
    end
end

function CharScreen:newChar(filepath)
    self.charData = CharData:new(filepath)
    self:newTexture()
    
    self.title = filepath
    self:setTitle()
end

function CharScreen:newPalette(filepath)
    self.paletteData = PaletteData:new(filepath)
    self.paletteData:padTo256()
    self:newTexture()
end

function CharScreen:newTexture()
    self.charTexture = self.charData:generateTexture(self.paletteData, self.columns, self.paletteOffset)
    
    self.dirty = true
end

function CharScreen:init(filepath)
    DropTargetScreen.init(self)

    self.paletteData = PaletteData:new()
    self.paletteData:loadStandard16()
    self.paletteData:padTo256()
    
    self:newChar(filepath)
end

function CharScreen:draw()
    love.graphics.draw(self.charTexture, 32, 32, 0, 2, 2)
end

return CharScreen
