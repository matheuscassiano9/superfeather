local test = require "src.utils.test"

local Screen = require "src.screens.screen"

local DropTargetScreen = Screen:makeClass
{
    handlers = {}
}

DropTargetScreen.filetypeMapping =
{
}

DropTargetScreen.foldertypeMapping =
{
}

function DropTargetScreen.handlers.filedropped(self, file)
    local path = file:getFilename()
    local badFile = true

    for _, mapping in ipairs(self.filetypeMapping) do
        if path:find(mapping.extension, -mapping.extension:len()) then
            mapping.handler(self, path)
            badFile = false
            break
        end
    end
    
    if badFile then
        self:badFileDrop(path)
    end

    self.dirty = true
end

function DropTargetScreen.handlers.directorydropped(self, path)
    local badFolder = true

    for _, mapping in ipairs(self.foldertypeMapping) do
        if path:find(mapping.extension, -mapping.extension:len()) then
            mapping.handler(self, path)
            badFolder = false
            break
        end
    end
    
    if badFolder then
        self:badFolderDrop(path)
    end

    self.dirty = true
end

function DropTargetScreen:init()
    Screen.init(self)
end

function DropTargetScreen:badFileDrop()
    test.unusedArgs(self)
end

function DropTargetScreen:badFolderDrop()
    test.unusedArgs(self)
end

return DropTargetScreen
