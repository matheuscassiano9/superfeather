local app = require "src.app"
local extramath = require "src.utils.extramath"
local test = require "src.utils.test"

local DropTargetScreen = require "src.screens.droptargetscreen"

local EditorScreen = DropTargetScreen:makeClass
{
    handlers =
    {
        filedropped = DropTargetScreen.handlers.filedropped
    },
    sidebarWidth = 240,
    contentPadding = 16,
    scrollbarWidth = 24,
    scrollbarPadding = 8,
    scrollbarHeight = 64,
    contentScroll = 0,
    contentScrollMin = 0,
    contentScrollMax = 0,
    contentScrollStep = 0,
}

function EditorScreen:init(file)
    test.unusedArgs(file)
    DropTargetScreen.init(self)
    
    self.undoStack = {}
    self.redoStack = {}
end

function EditorScreen:getContentDimensions()
    local width, height = love.graphics.getDimensions()

    local contentWidth = width - self.sidebarWidth - self.contentPadding - self.scrollbarWidth - self.scrollbarPadding
    local contentHeight = height - self.contentPadding * 2

    return contentWidth, contentHeight
end

function EditorScreen:getMouseRegion(x, y)
    local width, height = love.graphics.getDimensions()
    local contentWidth, contentHeight = self:getContentDimensions()
    local sidebarX = width - self.sidebarWidth

    local scrollbarX = self.contentPadding + contentWidth
    local region = nil

    if x >= sidebarX then
        region =
        {
            name = "sidebar",
            x = x - sidebarX,
            y = y,
            w = self.sidebarWidth,
            h = height,
        }
    elseif x >= scrollbarX and x < scrollbarX + self.scrollbarWidth then
        region =
        {
            name = "scrollbar",
        }
    elseif x >= self.contentPadding and x < sidebarX - self.contentPadding
           and y >= self.contentPadding and y < height - self.contentPadding then
        region =
        {
            name = "content",
            x = x - self.contentPadding,
            y = y - self.contentPadding,
            w = contentWidth,
            h = contentHeight,
        }
    end

    return region
end

function EditorScreen:getScrollbarPercent(y)
    if self.contentScrollMax <= self.contentScrollMin then return 0 end

    local _, contentHeight = self:getContentDimensions()
    local handleRange = contentHeight - self.scrollbarHeight

    return (y - self.contentPadding) / handleRange
end

function EditorScreen:getScrollbarY()
    local _, contentHeight = self:getContentDimensions()

    local handleMin = self.contentPadding
    local handleRange = contentHeight - self.scrollbarHeight
    local handleY = handleMin
                    + (self.contentScroll - self.contentScrollMin)
                    / (self.contentScrollMax - self.contentScrollMin) * handleRange

    return handleY
end

EditorScreen.globalHandlers =
{
    c = function(self, isRepeat)
        if isRepeat or not self.clipboardId then return end
        
        local clipboardData = "HeartcoreEditor," .. self.clipboardId .. "," .. self:copyToClipboard()

        love.system.setClipboardText(clipboardData)
    end,
    v = function(self, isRepeat)
        if isRepeat or not self.clipboardId then return end
        
        local expectedHeader = "HeartcoreEditor," .. self.clipboardId .. ","
        local clipboardData = love.system.getClipboardText()

        local header = clipboardData:sub(1, expectedHeader:len())

        if header == expectedHeader then
            local body = clipboardData:sub(expectedHeader:len() + 1)

            self:pasteFromClipboard(body)
        end
    end,
    z = function(self)
        if not love.keyboard.isDown("lshift", "rshift") then
            self:undo()
        else
            self:redo()
        end
    end,
}

function EditorScreen.handlers.keypressed(self, key, scancode, isRepeat)
    test.unusedArgs(scancode)
    local func

    if love.keyboard.isDown("lctrl", "rctrl") then
        func = self.globalHandlers[key] or self.keyHandlers[key]
    else
        func = self.keyHandlers[key]
    end

    if func then
        func(self, isRepeat)
    end
end

function EditorScreen.handlers.mousepressed(self, x, y, button)
    local region = self:getMouseRegion(x, y)

    if not region then return end
    
    self.clickedRegion = region.name

    if region.name == "scrollbar" then
        self:mousePressedScrollbar(y)
    elseif region.name == "sidebar" then
        self:mousePressedSidebar(region.x, region.y, button, region.w, region.h)
    elseif region.name == "content" then
        self:mousePressedContent(region.x, region.y, button, region.w, region.h)
    end
end

function EditorScreen.handlers.mousemoved(self, x, y)
    if self.scrolling then
        self:mouseMovedScrollbar(y)
        return
    end

    local region = self:getMouseRegion(x, y)

    if not region then return end

    if region.name == "sidebar" then
        self:mouseMovedSidebar(region.x, region.y, region.w, region.h)
    elseif region.name == "content" then
        self:mouseMovedContent(region.x, region.y, region.w, region.h)
    end
end

function EditorScreen.handlers.mousereleased(self, x, y, button)
    self.scrolling = false
    
    local region = self:getMouseRegion(x, y)

    if not region or region.name ~= self.clickedRegion then return end

    if region.name == "content" then
        self:mouseReleasedContent(region.x, region.y, button, region.w, region.h)
    end
end

function EditorScreen.handlers.wheelmoved(self, x, y)
    test.unusedArgs(x)
    local newScroll = self.contentScroll + (-1 * self.contentScrollStep * y)
    self.contentScroll = extramath.clampValue(self.contentScrollMin, self.contentScrollMax, newScroll)

    self.dirty = true
end

function EditorScreen:mouseMovedContent(x, y, button, width, height)
    test.unusedArgs(self, x, y, button, width, height)
end

function EditorScreen:mouseMovedSidebar(x, y, button, width, height)
    test.unusedArgs(self, x, y, button, width, height)
end

function EditorScreen:mouseMovedScrollbar(mouseY)
    if self.scrolling then
        local y = self:getScrollbarPercent(mouseY - self.scrollClickOffset)
        self.contentScroll = y * (self.contentScrollMax - self.contentScrollMin) + self.contentScrollMin
        self.contentScroll = extramath.clampValue(self.contentScrollMin, self.contentScrollMax, self.contentScroll)

        self.dirty = true
    end
end

function EditorScreen:mousePressedContent(x, y, button, width, height)
    test.unusedArgs(self, x, y, button, width, height)
end

function EditorScreen:mousePressedSidebar(x, y, button, width, height)
    test.unusedArgs(self, x, y, button, width, height)
end

function EditorScreen:mouseReleasedContent(x, y, button, width, height)
    test.unusedArgs(self, x, y, button, width, height)
end

function EditorScreen:mousePressedScrollbar(mouseY)
    self.scrolling = true
    self.scrollClickOffset = mouseY - self:getScrollbarY()
end

function EditorScreen:drawContent(width, height)
    test.unusedArgs(self, width, height)
    test.implementMe()
end

function EditorScreen:drawSidebar(width, height)
    test.unusedArgs(self, width, height)
    test.implementMe()
end

function EditorScreen:copyToClipboard()
    test.unusedArgs(self)
    return ""
end

function EditorScreen:pasteFromClipboard(clipboardData)
    test.unusedArgs(self, clipboardData)
end

function EditorScreen:doEditAction(action)
    if not action:isChange() then return end
    
    action:redo()
    
    self.redoStack = {}
    self.dirty = true
    
    local lastCheckpoint = self.undoStack[#self.undoStack]
    
    if not lastCheckpoint or (self.addUndoCheckpoint and #lastCheckpoint > 0) then
        lastCheckpoint = {}
        table.insert(self.undoStack, lastCheckpoint)
        self.addUndoCheckpoint = false
    end
    
    table.insert(lastCheckpoint, action)
    
    self.unsaved = true
end

function EditorScreen:undo()
    if #self.undoStack <= 0 then
        return
    end
    
    local checkpoint = table.remove(self.undoStack)
    
    for i = #checkpoint, 1, -1 do
        checkpoint[i]:undo()
    end
    
    table.insert(self.redoStack, checkpoint)
    self.dirty = true
end

function EditorScreen:redo()
    if #self.redoStack <= 0 then
        return
    end
    
    local checkpoint = table.remove(self.redoStack)
    
    for _, action in ipairs(checkpoint) do
        action:redo()
    end
    
    table.insert(self.undoStack, checkpoint)
    self.dirty = true
end

function EditorScreen:undoCheckpoint()
    self.addUndoCheckpoint = true
end

function EditorScreen:updateScrollInfo(min, max, step, handleHeight)
    local _, contentHeight = self:getContentDimensions()

    self.contentScrollMin = min
    self.contentScrollMax = max
    self.contentScrollStep = step
    self.scrollbarHeight = contentHeight * extramath.clampValue(0, 1, handleHeight)

    self.contentScroll = extramath.clampValue(self.contentScrollMin, self.contentScrollMax, self.contentScroll)
end

function EditorScreen:draw()
    local width, height = love.graphics.getDimensions()
    local sidebarX = width - self.sidebarWidth
    local contentWidth, contentHeight = self:getContentDimensions()

    if contentWidth > 0 and contentHeight > 0 then
        -- Draw content area
        love.graphics.push()

        love.graphics.setScissor(self.contentPadding, self.contentPadding, contentWidth, contentHeight)
        love.graphics.translate(self.contentPadding, self.contentPadding)

        self:drawContent(contentWidth, contentHeight)

        love.graphics.setScissor()
        love.graphics.pop()
    end

    if self.contentScrollMax > self.contentScrollMin then
        -- Draw scrollbar
        local scrollbarX = self.contentPadding + contentWidth
        local scrollbarLineX = scrollbarX + (self.scrollbarWidth / 2)

        local handleY = self:getScrollbarY()

        love.graphics.setColor(128, 128, 128)
        love.graphics.setLineWidth(3)
        love.graphics.line(scrollbarLineX, self.contentPadding, scrollbarLineX, self.contentPadding + contentHeight)

        love.graphics.setColor(255, 255, 255)
        love.graphics.rectangle("fill", scrollbarX + 3, handleY, self.scrollbarWidth - 6, self.scrollbarHeight)
    end

    -- Draw right pane shadow
    love.graphics.setColor(8, 8, 8, 128)
    love.graphics.draw(app.res.alphaGradient, sidebarX, 0, math.pi / 2, height, 16/256)
    love.graphics.setColor(255, 255, 255, 255)

    -- Draw right pane
    love.graphics.push()
    love.graphics.setScissor(sidebarX, 0, self.sidebarWidth, height)
    love.graphics.translate(sidebarX, 0)

    love.graphics.clear(16, 16, 16)

    self:drawSidebar(self.sidebarWidth, height)

    love.graphics.setScissor()
    love.graphics.pop()
end

return EditorScreen
