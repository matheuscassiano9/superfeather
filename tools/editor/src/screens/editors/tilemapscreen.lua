local app = require "src.app"
local extramath = require "src.utils.extramath"
local test = require "src.utils.test"

local EditorScreen = require "src.screens.editors.editorscreen"
local EditorAction = require "src.screens.editors.editoraction"
local TilePickerScreen = require "src.screens.editors.tilepickerscreen"
local TilemapData = require "src.dataobjs.tilemapdata"
local PaletteData = require "src.dataobjs.palettedata"
local CharData = require "src.dataobjs.chardata"
local SavePromptScreen = require "src.screens.savepromptscreen"

local textureColumns = 16
local maxTiles = 1024

local helpString = "S - Save"
                   .. "\nCtrl+C/Ctrl+V - Copy/paste"
                   .. "\nArrows/Left click - Select tile"
                   .. "\nShift - Multiselect"
                   .. "\nB/N - Adjust char index"
                   .. "\n0-7 - Set palette"
                   .. "\nH, V - Toggle H/V flip"
                   .. "\nP - Toggle priority"
                   .. "\n\n-/= - Toggle 32/64-width display"
                   .. "\nL - Toggle low priority tile display"

local ChangeTileAction = EditorAction:makeClass
{
    
}

function ChangeTileAction:init(tilemapScreen, tileId, index, palette, priority, hflip, vflip, select)
    self.tilemapScreen = tilemapScreen
    self.tileId = tileId
    
    if select then
        self.selectId = tilemapScreen.selectedTile
    end
    
    self.oldTile = {}
    local tile = self.tilemapScreen.tilemapData.tiles[self.tileId]
    
    for k, v in pairs(tile) do
        self.oldTile[k] = v
    end
    
    self.newTile =
    {
        index = index % 1024,
        palette = palette % 8,
        priority = priority,
        hflip = hflip,
        vflip = vflip
    }
end

function ChangeTileAction:undo()
    local tile = self.tilemapScreen.tilemapData.tiles[self.tileId]
    
    for k, v in pairs(self.oldTile) do
        tile[k] = v
    end
    
    if self.selectId then
        self.tilemapScreen:setSelectedTile(self.selectId)
    end
end

function ChangeTileAction:redo()
    local tile = self.tilemapScreen.tilemapData.tiles[self.tileId]
    
    for k, v in pairs(self.newTile) do
        tile[k] = v
    end
    
    if self.selectId then
        self.tilemapScreen:setSelectedTile(self.selectId)
    end
end

function ChangeTileAction:isChange()
    for k, v in pairs(self.newTile) do
        if self.oldTile[k] ~= v then return true end
    end
    
    return false
end

local TilemapScreen = EditorScreen:makeClass
{
    handlers =
    {
        keypressed = EditorScreen.handlers.keypressed,
        filedropped = EditorScreen.handlers.filedropped,
        mousepressed = EditorScreen.handlers.mousepressed,
        mousemoved = EditorScreen.handlers.mousemoved,
        wheelmoved = EditorScreen.handlers.wheelmoved,
    },
    submapColumns = 1,
    clipboardId = "Tilemap",
    highTilesOnly = false
}

TilemapScreen.filetypeMapping =
{
    {
        extension = ".map",
        description = "SNES tilemap",
        handler = function(self, file)
            self:newTilemap(file)
        end
    },
    {
        extension = ".pic",
        description = "SNES char data",
        handler = function(self, file)
            self:newChar(file)
        end
    },
    {
        extension = ".pic2",
        description = "SNES char data (2bpp)",
        handler = function(self, file)
            self:newChar(file)
        end
    },
    {
        extension = ".pal",
        description = "SNES palette",
        handler = function(self, file)
            self:newPalette(file)
        end
    },
}

TilemapScreen.keyHandlers =
{
    s = function(self, isRepeat)
        if not isRepeat then
            self.tilemapData:saveToFile()
            self.unsaved = false
        end
    end,
    left = function(self)
        self:setSelectedTileRelative(-1, 0)
        self.needScroll = true
    end,
    right = function(self)
        self:setSelectedTileRelative(1, 0)
        self.needScroll = true
    end,
    up = function(self)
        self:setSelectedTileRelative(0, -1)
        self.needScroll = true
    end,
    down = function(self)
        self:setSelectedTileRelative(0, 1)
        self.needScroll = true
    end,
    ["-"] = function(self)
        self.submapColumns = 1
        self.needScroll = true
        self.dirty = true
    end,
    ["="] = function(self)
        self.submapColumns = 2
        self.needScroll = true
        self.dirty = true
    end,
    l = function(self)
        self.highTilesOnly = not self.highTilesOnly
        self.dirty = true
    end,
    n = function(self)
        self:setTileCharRelative(1)
    end,
    b = function(self)
        self:setTileCharRelative(-1)
    end,
    ["`"] = function(self)
        self:setTilePalette(0)
    end,
    ["0"] = function(self)
        self:setTilePalette(0)
    end,
    ["1"] = function(self)
        self:setTilePalette(1)
    end,
    ["2"] = function(self)
        self:setTilePalette(2)
    end,
    ["3"] = function(self)
        self:setTilePalette(3)
    end,
    ["4"] = function(self)
        self:setTilePalette(4)
    end,
    ["5"] = function(self)
        self:setTilePalette(5)
    end,
    ["6"] = function(self)
        self:setTilePalette(6)
    end,
    ["7"] = function(self)
        self:setTilePalette(7)
    end,
    p = function(self)
        self:toggleTilePriority()
    end,
    h = function(self)
        self:toggleTileHflip()
    end,
    v = function(self)
        self:toggleTileVflip()
    end,
    tab = function(self)
        if self.charTexture and self.quads and self.pickerData then
            app.pushScreen(TilePickerScreen:new(self.charTexture, self.quads, self.pickerData))
        end
    end,
    q = function(self)
        local tile = self.tilemapData.tiles[self.selectedTile]
        
        self.pickerData.index = tile.index
        self.pickerData.palette = tile.palette
        self.pickerData.priority = tile.priority
        self.pickerData.hflip = tile.hflip
        self.pickerData.vflip = tile.vflip
        
        self.dirty = true
    end,
    space = function(self)
        self:undoCheckpoint()
        self:paintTile(self.selectedTile, true)
    end,
}

function TilemapScreen:mousePressedContent(x, y, button, w, h)
    test.unusedArgs(h)
    
    if button == 1 then
        self:mouseSelectTile(x, y, w)
    elseif button == 2 then
        self.mousePainting = true
        self:undoCheckpoint()
        self:mousePaintTile(x, y, w)
    end
end

function TilemapScreen:mouseMovedContent(x, y, w, h)
    test.unusedArgs(h)
    
    if love.mouse.isDown(1) then
        self:mouseSelectTile(x, y, w)
    elseif love.mouse.isDown(2) and self.mousePainting then
        self:mousePaintTile(x, y, w)
    else
        self.mousePainting = false
    end
end

function TilemapScreen:screenToGridCoords(x, y, w)
    local scale = self:getTileScale(w)
    return math.floor(x / 8 / scale), math.floor((y + self.contentScroll) / 8 / scale)
end

function TilemapScreen:mouseSelectTile(x, y, w)
    local tileX, tileY = self:screenToGridCoords(x, y, w)
    local index = self:coordsToTileIndex(tileX, tileY)
    
    if love.keyboard.isDown("lshift", "rshift") then
        if not self.dragX1 then
            self.dragX1, self.dragY1 = tileX, tileY
        end
        self.dragX2, self.dragY2 = tileX, tileY
        self.dirty = true
    else
        self:setSelectedTile(index)
    end
end

function TilemapScreen:mousePaintTile(x, y, w)
    local tileX, tileY = self:screenToGridCoords(x, y, w)
    local index = self:coordsToTileIndex(tileX, tileY)
    
    self:paintTile(index)
end

function TilemapScreen:paintTile(index, isKeyboard)
    if self.hasMultiSelection and not self.multiSelect[index] then
        return
    end
    
    self:doEditAction(ChangeTileAction:new(self, index, self.pickerData.index,
                          self.pickerData.palette, self.pickerData.priority,
                          self.pickerData.hflip, self.pickerData.vflip, isKeyboard))
    
    self.dirty = true
end

function TilemapScreen.handlers.mousereleased(self, x, y, button)
    EditorScreen.handlers.mousereleased(self, x, y, button)
    
    if button == 1 and self.dragX1 then
        local x1 = math.min(self.dragX1, self.dragX2)
        local x2 = math.max(self.dragX1, self.dragX2)
        local y1 = math.min(self.dragY1, self.dragY2)
        local y2 = math.max(self.dragY1, self.dragY2)
        
        for y = y1, y2 do
            for x = x1, x2 do
                local index = self:coordsToTileIndex(x, y, true)
                
                if index ~= -1 then
                    self:multiSelectTile(index)
                end
            end
        end
        
        local index = self:coordsToTileIndex(self.dragX1, self.dragY1, true)
        if index ~= -1 then
            self:multiSelectTile(index)
        end
        
        self.dragX1 = nil
    end
    
    self.mousePainting = false
    self.dirty = true
end

function TilemapScreen:setTileChar(index)
    local tile = self.tilemapData.tiles[self.selectedTile]
    
    self:undoCheckpoint()
    self:doEditAction(ChangeTileAction:new(self, self.selectedTile, index,
                        tile.palette, tile.priority, tile.hflip, tile.vflip, true))
end

function TilemapScreen:setTileCharRelative(offset)
    if not self.hasMultiSelection then
        local tile = self.tilemapData.tiles[self.selectedTile]
        
        self:undoCheckpoint()
        self:doEditAction(ChangeTileAction:new(self, self.selectedTile, tile.index + offset,
                          tile.palette, tile.priority, tile.hflip, tile.vflip, true))
    end
end

function TilemapScreen:setTilePalette(palette)
    if not self.hasMultiSelection then
        local tile = self.tilemapData.tiles[self.selectedTile]
        
        self:undoCheckpoint()
        self:doEditAction(ChangeTileAction:new(self, self.selectedTile, tile.index,
                          palette, tile.priority, tile.hflip, tile.vflip, true))
    else
        self:undoCheckpoint()
        for k, _ in pairs(self.multiSelect) do
            local tile = self.tilemapData.tiles[k]
            
            self:doEditAction(ChangeTileAction:new(self, k, tile.index,
                    palette, tile.priority, tile.hflip, tile.vflip, true))
        end
    end
    self.dirty = true
end

function TilemapScreen:toggleTilePriority()
    if not self.hasMultiSelection then
        local tile = self.tilemapData.tiles[self.selectedTile]
        
        self:undoCheckpoint()
        self:doEditAction(ChangeTileAction:new(self, self.selectedTile, tile.index,
                    tile.palette, not tile.priority, tile.hflip, tile.vflip, true))
    else
        self:undoCheckpoint()
        local newPriority = false
        
        for k, _ in pairs(self.multiSelect) do
            local tile = self.tilemapData.tiles[k]
            
            if not tile.priority then
                newPriority = true
                break
            end
        end
        
        for k, _ in pairs(self.multiSelect) do
            local tile = self.tilemapData.tiles[k]
            
            self:doEditAction(ChangeTileAction:new(self, k, tile.index,
                        tile.palette, newPriority, tile.hflip, tile.vflip, true))
        end
    end
    self.dirty = true
end

function TilemapScreen:toggleTileHflip()
    if not self.hasMultiSelection then
        local tile = self.tilemapData.tiles[self.selectedTile]
        
        self:undoCheckpoint()
        self:doEditAction(ChangeTileAction:new(self, self.selectedTile, tile.index,
                    tile.palette, tile.priority, not tile.hflip, tile.vflip, true))
    else
        self:undoCheckpoint()
        for k, _ in pairs(self.multiSelect) do
            local tile = self.tilemapData.tiles[k]
            
            self:doEditAction(ChangeTileAction:new(self, k, tile.index,
                        tile.palette, tile.priority, not tile.hflip, tile.vflip, true))
        end
    end
    self.dirty = true
end

function TilemapScreen:toggleTileVflip()
    if not self.hasMultiSelection then
        local tile = self.tilemapData.tiles[self.selectedTile]
        
        self:undoCheckpoint()
        self:doEditAction(ChangeTileAction:new(self, self.selectedTile, tile.index,
                    tile.palette, tile.priority, tile.hflip, not tile.vflip, true))
    else
        self:undoCheckpoint()
        for k, _ in pairs(self.multiSelect) do
            local tile = self.tilemapData.tiles[k]
            
            self:doEditAction(ChangeTileAction:new(self, k, tile.index,
                        tile.palette, tile.priority, tile.hflip, not tile.vflip, true))
        end
    end
    self.dirty = true
end

function TilemapScreen:setSelectedTile(index, keepMulti)
    if not keepMulti then
        self.multiSelect = {}
        self.hasMultiSelection = false
    end
    self.selectedTile = extramath.clampValue(1, #self.tilemapData.tiles, index)
    self.dirty = true
end

function TilemapScreen:multiSelectTile(index)
    self.multiSelect[index] = true
    self.hasMultiSelection = true
    self.selectedTile = extramath.clampValue(1, #self.tilemapData.tiles, index)
    self.dirty = true
end

function TilemapScreen:setSelectedTileRelative(x, y)
    local tileX, tileY = self:tileIndexToCoords(self.selectedTile)
    local index = self:coordsToTileIndex(tileX + x, tileY + y)
    
    if love.keyboard.isDown("space") then
        self:setSelectedTile(index, true)
        self:undoCheckpoint()
        self:paintTile(index, true)
    elseif love.keyboard.isDown("lshift", "rshift") then
        local oldIndex = self:coordsToTileIndex(tileX, tileY)
        self:multiSelectTile(oldIndex)
        self:multiSelectTile(index)
    else
        self:setSelectedTile(index)
    end
end

function TilemapScreen:tileIndexToCoords(index)
    index = index - 1
    
    local tileX = (index % 32)
    local tileY = math.floor(index / 32) % 32
    tileX = tileX + (math.floor(index / 1024) % self.submapColumns) * 32
    tileY = tileY + math.floor(index / 1024 / self.submapColumns) * 32
    
    return tileX, tileY
end

function TilemapScreen:tileIndexToScreenCoords(index, scale)
    local tileX, tileY = self:tileIndexToCoords(index)
    
    return tileX * 8 * scale, tileY * 8 * scale - self.contentScroll
end

function TilemapScreen:coordsToTileIndex(tileX, tileY, noClamp)
    local numSubmaps = math.ceil(#self.tilemapData.tiles / 1024)
    local totalHeight = math.ceil(numSubmaps / self.submapColumns)
    local bottomRow = totalHeight * 32 - 1
    local columnsThisRow = self.submapColumns
    
    if not noClamp and tileY >= bottomRow - 32 then
        local remainder = numSubmaps % self.submapColumns
        columnsThisRow = remainder == 0 and self.submapColumns or remainder
    end
    
    local oldTileX = tileX
    local oldTileY = tileY
    tileX = extramath.clampValue(0, columnsThisRow * 32 - 1, tileX)
    tileY = extramath.clampValue(0, bottomRow, tileY)
    
    if noClamp and (tileX ~= oldTileX or tileY ~= oldTileY) then
        return -1
    end
    
    local index = (tileX % 32) + math.floor(tileX / 32) * 1024
    index = index + (tileY % 32) * 32 + math.floor(tileY / 32) * 1024 * self.submapColumns
    
    return index + 1
end

function TilemapScreen:getTileScale(width)
    local scale = width / 32 / 8 / self.submapColumns
    
    if scale >= 1 then scale = math.floor(scale) end
    
    return scale
end

function TilemapScreen:copyToClipboard()
    local clipboardData = ""
    
    if not self.hasMultiSelection then
        local tile = self.tilemapData.tiles[self.selectedTile]
        clipboardData = "0,0," .. TilemapData.tileToWord(tile)
    else
        local originX, originY = self:tileIndexToCoords(self.selectedTile)
        
        for k, _ in pairs(self.multiSelect) do
            local tile = self.tilemapData.tiles[k]
            local tileX, tileY = self:tileIndexToCoords(k)
            clipboardData = clipboardData
                            .. tostring(tileX - originX) .. ","
                            .. tostring(tileY - originY) .. ","
                            .. TilemapData.tileToWord(tile) .. ","
        end
    end
    
    return clipboardData
end

function TilemapScreen:pasteFromClipboard(clipboardData)
    local items = {}
    local originX, originY = self:tileIndexToCoords(self.selectedTile)
    
    clipboardData:gsub("([^,]+)", function(item)
        table.insert(items, item)
    end)
    
    local multiPaste = (#items > 3)
    
    if multiPaste then
        self.multiSelect = {}
    end
    
    self:undoCheckpoint()
    
    for i = 1, #items - 2, 3 do
        local tileX = items[i] + originX
        local tileY = items[i+1] + originY
        local index = self:coordsToTileIndex(tileX, tileY, true)
        
        if index ~= -1 then
            local newTile = TilemapData.wordToTile(tostring(items[i+2]))
            
            self:doEditAction(ChangeTileAction:new(self, index, newTile.index,
                        newTile.palette, newTile.priority, newTile.hflip, newTile.vflip))
            
            if multiPaste then
                self.multiSelect[index] = true
                self.hasMultiSelection = true
            end
        end
    end
    
    self.dirty = true
end

function TilemapScreen:newTilemap(filepath)
    local loadFunc = function()
        self.tilemapData = TilemapData:new(filepath)
        
        self.title = filepath
        self:setTitle()
        
        self.dirty = true
        self.unsaved = false
    end
    
    if self.unsaved then
        app.pushScreen(SavePromptScreen:new("load", loadFunc))
    else
        loadFunc()
    end
end

function TilemapScreen:newTextures()
    if not self.charData then
        return
    end
    
    self.paletteData:padTo256()
    
    local paletteOffset = 0
    local fullImageData = love.image.newImageData(textureColumns*8*8, maxTiles*8/textureColumns)
    
    for i = 0, 7 do
        local imageData, transparent = self.charData:generateImageData(self.paletteData, textureColumns, paletteOffset)
        paletteOffset = paletteOffset + 2^self.charData.bitDepth
        fullImageData:paste(imageData, i*textureColumns*8, 0, 0, 0, imageData:getWidth(), imageData:getHeight())
        
        if i == 0 then
            self.transparentTiles = transparent
        end
    end
    
    self.charTexture = love.graphics.newImage(fullImageData)
    
    self.batch = love.graphics.newSpriteBatch(self.charTexture, maxTiles, "stream")
    
    self.quads = {}
    local imageWidth, imageHeight = self.charTexture:getDimensions()
    local imagePaletteWidth = imageWidth / 8
    
    for i = 0, 7 do
        self.quads[i] = {}
        local xOffset = i * imagePaletteWidth
        
        for j = 0, #self.charData.tiles - 1 do
            local x = j * 8 % imagePaletteWidth + xOffset
            local y = math.floor(j / textureColumns) * 8
            self.quads[i][j] = love.graphics.newQuad(x, y, 8, 8, imageWidth, imageHeight)
        end
    end
    
    self.dirty = true
end

function TilemapScreen:newChar(filepath)
    self.charData = CharData:new(filepath)
    self:newTextures()
end

function TilemapScreen:newPalette(filepath)
    self.paletteData = PaletteData:new(filepath)
    self:newTextures()
end

function TilemapScreen:init(filepath)
    EditorScreen.init(self)
    
    self.selectedTile = 1
    self.hasMultiSelection = false
    self.multiSelect = {}
    
    self.pickerData =
    {
        index = 0,
        palette = 0,
        priority = false,
        hflip = false,
        vflip = false,
    }
    
    self.paletteData = PaletteData:new()
    self.paletteData:loadStandard16()
    self:newTilemap(filepath)
end

function TilemapScreen:drawContent(width, height)
    if not self.tilemapData or not self.charData then
        love.graphics.print("Please provide char data (and a palette).", 32, 32)
        return
    end
    
    local scale = self:getTileScale(width)
    
    -- Various metrics we'll need
    local numSubmaps = math.ceil(#self.tilemapData.tiles / 1024)
    local submapHeight = 8 * 32 * scale
    local totalHeight = math.ceil(numSubmaps / self.submapColumns) * submapHeight
    local scrollSize = math.max(0, totalHeight - height)
    local submapsVisible = math.ceil(height / submapHeight * self.submapColumns)
    
    local tileX, tileY = self:tileIndexToCoords(self.selectedTile)
    test.unusedArgs(tileX)
    
    -- Update scroll if keyboard control used
    if self.needScroll then
        local maxY = tileY * 8 * scale
        local minY = math.max(0, (tileY + 1) * 8 * scale - height)
        
        self.contentScroll = extramath.clampValue(minY, maxY, self.contentScroll)
        self.needScroll = false
    end
    
    self:updateScrollInfo(0, scrollSize, 32, scrollSize > 0 and height / totalHeight or 1)
    
    -- Drawing the actual tiles themselves
    local firstSubmap = math.floor(self.contentScroll / submapHeight)
    local lastMap = math.min(firstSubmap + submapsVisible, numSubmaps - 1)
    local multiSelectPos = {}
    
    -- We draw by one "submap" (32x32 tile region) at a time due to their layout
    for submap = firstSubmap, lastMap do
        local submapX = submap % self.submapColumns * submapHeight
        local submapY = submapHeight * math.floor(submap / self.submapColumns) - self.contentScroll
        local submapTile = submap * 1024
        
        self.batch:clear()
        
        love.graphics.push()
        love.graphics.translate(submapX, submapY)
        love.graphics.scale(scale)
        
        love.graphics.setColor(128, 128, 128)
        
        -- 1024 tiles in a submap
        for i = 0, 1023 do
            local index = i + submapTile + 1
            local tile = self.tilemapData.tiles[index]
            
            if not tile then break end
            
            local quad = self.quads[tile.palette]
            local x = (i % 32) * 8
            local y = math.floor(i / 32) * 8
            local transparent = self.transparentTiles[tile.index]
            
            if quad and quad[tile.index] and not transparent and (not self.highTilesOnly or tile.priority) then
                -- Tile can be drawn
                local w = tile.hflip and -1 or 1
                local h = tile.vflip and -1 or 1
                
                -- Using offset of 4 so that flipping doesn't move the tile
                self.batch:add(quad[tile.index], x + 4, y + 4, 0, w, h, 4, 4)
            elseif self.highTilesOnly and tile.priority then
                -- Tile is full transparent, display as an X if high priority and priority viewing enabled
                local x1, y1 = x + 1, y + 1
                local x2, y2 = x + 7, y + 7
                
                love.graphics.setLineWidth(0.25)
                love.graphics.setLineStyle("smooth")
                love.graphics.line(x1, y1, x2, y2)
                love.graphics.line(x2, y1, x1, y2)
            end
            
            -- List of multi-selected tiles, stored in array for later because we need to remember their
            --  calculated positions
            if self.hasMultiSelection and self.multiSelect[index] then
                local pos = {x = submapX + x * scale, y = submapY + y * scale}
                table.insert(multiSelectPos, pos)
            end
        end
        
        love.graphics.setColor(255, 255, 255)
        love.graphics.draw(self.batch, 0, 0, 0, 1, 1)
        love.graphics.pop()
    end
    
    -- Draw multiselection
    
    love.graphics.setColor(255, 255, 255, 128)
    
    for _, v in ipairs(multiSelectPos) do
        love.graphics.rectangle("fill", v.x, v.y, scale * 8, scale * 8)
    end
    
    -- Draw single selection indicator
    
    tileX, tileY = self:tileIndexToScreenCoords(self.selectedTile, scale)
    
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setLineWidth(2)
    love.graphics.setLineStyle("smooth")
    love.graphics.setScissor()
    love.graphics.rectangle("line", tileX, tileY, scale * 8, scale * 8)
    
    -- Draw drag selection rectangle
    
    if self.dragX1 then
        love.graphics.push()
        love.graphics.translate(0, -self.contentScroll)
        love.graphics.scale(scale * 8)
        
        local x = math.min(self.dragX1, self.dragX2)
        local y = math.min(self.dragY1, self.dragY2)
        local w = math.abs(self.dragX1 - self.dragX2) + 1
        local h = math.abs(self.dragY1 - self.dragY2) + 1
        
        love.graphics.setLineWidth(0.125)
        love.graphics.rectangle("line", x, y, w, h)
        love.graphics.setColor(255, 255, 255, 128)
        love.graphics.rectangle("fill", x, y, w, h)
        love.graphics.setColor(255, 255, 255, 255)
        
        love.graphics.pop()
    end
end

function TilemapScreen:drawSidebarTile(tile, x, y)
    if not self.quads then return end
    
    local function yesNo(boolean)
        return boolean and "Yes" or "No"
    end
    
    local function highLow(boolean)
        return boolean and "High" or "Low"
    end
    
    local quad = self.quads[tile.palette]
    local w = tile.hflip and -1 or 1
    local h = tile.vflip and -1 or 1
    
    if quad and quad[tile.index] then
        love.graphics.draw(self.charTexture, quad[tile.index], x + 32, y + 34, 0, w * 8, h * 8, 4, 4)
    end
    
    love.graphics.setFont(app.res.uifontSmall)
    love.graphics.print("Char index\nPalette\nH-flip\nV-flip\nPriority", x + 80, y)
    love.graphics.print("" .. tile.index
                        .. "\n" .. tile.palette
                        .. "\n" .. yesNo(tile.hflip)
                        .. "\n" .. yesNo(tile.vflip)
                        .. "\n" .. highLow(tile.priority), x + 160, y)
end

function TilemapScreen:drawSidebar(width, height)
    test.unusedArgs(width)
    if not self.tilemapData or not self.charData then return end
    
    love.graphics.setFont(app.res.uifont)
    love.graphics.setColor(255, 255, 255)
    love.graphics.print("Selected Tile", 16, 16)
    
    love.graphics.setFont(app.res.uifontSmall)
    local index = self.selectedTile - 1
    local tileX, tileY = self:tileIndexToCoords(self.selectedTile)
    
    love.graphics.print("Position " .. index .. "\t\t(row " .. tileX .. ", col " .. tileY .. ")", 16, 48)
    
    local tile = self.tilemapData.tiles[self.selectedTile]
    
    self:drawSidebarTile(tile, 16, 80)
    
    love.graphics.setFont(app.res.uifont)
    love.graphics.print("Drawing Tile  (RMB / Space)", 16, 176)
    
    self:drawSidebarTile(self.pickerData, 16, 208)
    
    love.graphics.print("Tab - Pick", 16, 304)
    love.graphics.print("Q - Use Selected", 96, 304)
    
    love.graphics.setFont(app.res.uifont)
    if self.highTilesOnly then
        love.graphics.setColor(255, 128, 128)
        love.graphics.print("Showing high tiles only", 16, 336)
        love.graphics.setColor(255, 255, 255)
    end
    
    love.graphics.setFont(app.res.uifontSmall)
    if height >= 560 then
        love.graphics.print(helpString, 16, height - (11*16) - 16)
    end
end

return TilemapScreen
