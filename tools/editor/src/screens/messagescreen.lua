
local app = require "src.app"
local cabin = require "src.utils.cabin"
local test = require "src.utils.test"

local Screen = require "src.screens.screen"

local MessageScreen = Screen:makeClass
{
    handlers = {},
    message = ""
}

function MessageScreen:init(message)
    Screen.init(self)
    
    if not message or type(message) ~= "string" then
        error("Message wasn't supplied!")
    end
    
    self.message = message
    cabin.warning(message)
end

function MessageScreen.handlers.keypressed(self, key, scancode, isRepeat)
    test.unusedArgs(self, scancode, isRepeat)
    
    if key ~= "escape" then
        app.popScreen()
    end
end

function MessageScreen:draw()
    love.graphics.setColor(255, 255, 255)
    
    love.graphics.setFont(app.res.uifont)
    local width = love.graphics.getDimensions()
    
    love.graphics.printf(self.message .. "\n\nPress any key", 32, 32, width - 64)
end

return MessageScreen
