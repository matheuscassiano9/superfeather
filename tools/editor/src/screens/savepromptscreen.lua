
local app = require "src.app"
local test = require "src.utils.test"

local Screen = require "src.screens.screen"

local SavePromptScreen = Screen:makeClass
{
    handlers = {}
}

local _promptMessages =
{
    quit = "Are you sure you want to quit?",
    pop = "Are you sure you want to leave this editor?",
    load = "Are you sure you want to load a different file?"
}

function SavePromptScreen:init(actionType, actionFunc)
    Screen.init(self)
    
    self.actionType = actionType
    self.actionFunc = actionFunc
end

function SavePromptScreen.handlers.keypressed(self, key, scancode, isRepeat)
    test.unusedArgs(self, scancode, isRepeat)
    
    if key == "n" then
        app.popScreen()
    elseif key == "y" then
        if self.actionFunc then self.actionFunc() end
        app.popScreen()
    end
end

function SavePromptScreen:draw()
    love.graphics.setColor(255, 255, 255)
    
    love.graphics.setFont(app.res.uifontLarge)
    local width = love.graphics.getDimensions()
    
    local message = _promptMessages[self.actionType] or "Are you sure?"
    
    love.graphics.printf(message .. "\nYou have unsaved changes.", 32, 32, width - 64)
    
    love.graphics.setFont(app.res.uifontSemilarge)
    love.graphics.printf("Y to leave\t\t\t\t\tN to cancel\n", 64, 144, width - 64)
end

return SavePromptScreen
