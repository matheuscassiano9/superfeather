--[[
See LICENSE for important information on distribution.
--]]


--[[
OOPS! A small Lua OOP lib.

A reasonably fast OOP system. Very simple. Fit for game use. Doesn't enforce
encapsulation, unfortunately.
--]]

local test = require "src.utils.test"

local oops = {}

-- Base object table
oops.BaseObject = {}

--[[
    Copies the class member table from another class or forms a new one.
    Use this to define the member variables of a class, as well as the class
    you are subclassing from.
--]]
function oops.BaseObject:makeClass(obj)

    local newObj = obj or {}
    setmetatable(newObj, self)
    self.__index = self
    
    self.__isInstance = false

    return newObj

end

--[[
    Class object constructor. Define this for your class with any number of
    variables, and then call new() with matching parameters when you want a
    new object of that type. Call init on the superclass if you need to perform
    a construction on it as well:
    
    function Subclass:init(x, y, z)
        Superclass.init(self, x, y)
        
        -- Subclass initializations go here
    end
--]]
function oops.BaseObject:init()
    test.unusedArgs(self)
end

-- Returns the class that this object is an instance of.
function oops.BaseObject:getClass()
    if self.__isInstance then
        return getmetatable(self)
    else
        return self
    end
end

-- Constructs a new object of the specified class. Returns a reference to
--  the new object.
function oops.BaseObject:new(...)
    
    local newObj = {}
    local arg = {...}
    
    setmetatable(newObj, self)
    self.__index = self
    newObj.__isInstance = true
    
    newObj:init(unpack(arg))
    
    return newObj

end

-- Tests whether or not the object is an instance of some class. Returns true if
--  the object is an instance of the given class or one of its subclasses.
--  Otherwise returns false.
function oops.BaseObject:instanceOf(class)
    local isTrue = false
    local mt = self
    
    while mt do
        if mt == class then
            isTrue = true
            break
        end
        
        mt = getmetatable(mt)
    end
    
    return isTrue
end

-- Returns true if the object is an instance of a class, and false if it is a
--  class definition.
function oops.BaseObject:isInstance()
    return self.__isInstance
end


return oops
