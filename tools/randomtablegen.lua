
local bit = require "bit"

local orderedValues = {}
local repetitions = tonumber(arg[1]) or 1

math.randomseed(os.time())

for j = 1, repetitions do
    for i = 0, 255 do
        table.insert(orderedValues, i)
        -- Shuffle RNG a bit
        math.random()
    end
end

local shuffledValues = {}

while #orderedValues > 0 do
    local pos = math.random(1, #orderedValues)
    
    table.insert(shuffledValues, orderedValues[pos])
    table.remove(orderedValues, pos)
end

local hexString = ".byte "

for i, v in ipairs(shuffledValues) do
    
    if i % 8 ~= 1 then
        hexString = hexString .. ", "
    end
    
    hexString = hexString .. "$" .. bit.tohex(v, 2)
    
    if i % 8 == 0 and i ~= #shuffledValues then
        hexString = hexString .. "\n.byte "
    end
end

print(hexString)
